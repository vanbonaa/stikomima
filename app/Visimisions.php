<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visimisions extends Model
{
    protected $fillable = ['judul','visi','misi','gambar','slug'];
}
