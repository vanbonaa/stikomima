<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dosens extends Model
{
    protected $fillable = ['name','gambar','detail','slug'];
}
