<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advertisings extends Model
{
    protected $fillable = ['name','visimisi','program','karir','pdf','gambar','slug'];
}
