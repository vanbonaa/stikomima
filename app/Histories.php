<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Histories extends Model
{
    protected $fillable = ['name','keterangan','gambar','slug'];
}
