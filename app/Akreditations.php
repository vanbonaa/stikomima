<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Akreditations extends Model
{
    protected $fillable = ['name','status','no_surat','pdf','keterangan','slug'];
}
