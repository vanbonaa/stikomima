<?php

namespace App\Http\Middleware;
use Illuminate\Http\Request;
use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, string $role)
    {
        if ($role == 1 && auth()->user()->tipe != 1) {
            abort(403);
        }

        if ($role == 0 && auth()->user()->tipe != 0) {
            abort(403);
        }
        
        return $next($request);
    }
}
