<?php

namespace App\Http\Controllers;

use App\Histories;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class HistoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $histories = Histories::paginate(10);
        return view('admin.histories.index', compact('histories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.histories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'keterangan' => 'required',
            'gambar' => 'required'
        ]);

        $gambar = $request->gambar;
        $new_gambar = time().$gambar->getClientOriginalName();

        $histories = Histories::create([
            'name' => $request->name,
            'keterangan' => $request->keterangan,
            'gambar' => 'public/uploads/profile/histories/'.$new_gambar,
            'slug' => Str::slug($request->name)
        ]);
        
        $gambar->move('public/uploads/profile/histories/', $new_gambar);
        return redirect()->route('histories.index')->with('success', 'Data Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $histories = Histories::findorfail($id);
        return view('admin.histories.edit', compact('histories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'keterangan' => 'required',
            // 'gambar' => 'required'
        ]);
        $histories = Histories::findorfail($id);
        if ($request->has('gambar')) {
            $gambar = $request->gambar;
            $new_gambar = time().$gambar->getClientOriginalName();
            $gambar->move('public/uploads/profile/histories/', $new_gambar);

            $histories_data = [
                'name' => $request->name,
                'keterangan' => $request->keterangan,
                'gambar' => 'public/uploads/profile/histories/'.$new_gambar,
                'slug' => Str::slug($request->judul)
            ];    
        }
        else{
            $histories_data = [
                'name' => $request->name,
                'keterangan' => $request->keterangan,
                'slug' => Str::slug($request->judul)
            ];
     
        }
        
        $histories->update($histories_data);

        return redirect()->route('histories.index')->with('success', 'Data Berhasil Di Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $histories = Histories::findorfail($id);
        $histories->delete();

        return redirect()->back()->with('success', 'Data Berhasil Dihapus');
    }
}
