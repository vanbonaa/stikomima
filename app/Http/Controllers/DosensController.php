<?php

namespace App\Http\Controllers;

use App\Dosens;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class DosensController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dosens = Dosens::paginate(10);
        return view('admin.dosens.index', compact('dosens'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.dosens.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'gambar' => 'required'
        ]);

        $gambar = $request->gambar;
        $new_gambar = time().$gambar->getClientOriginalName();

        $dosens = Dosens::create([
            'name' => $request->name,
            'detail' => $request->detail,
            'gambar' => 'public/uploads/dosen/'.$new_gambar,
            'slug' => Str::slug($request->name)
        ]);
        
        $gambar->move('public/uploads/dosen/', $new_gambar);
        return redirect()->route('dosens.index')->with('success', 'Data Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dosens = Dosens::findorfail($id);
        return view('admin.dosens.edit', compact('dosens'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'detail' => 'required',
            'gambar' => 'required'
        ]);
        $dosens = Dosens::findorfail($id);
        if ($request->has('gambar')) {
            $gambar = $request->gambar;
            $new_gambar = time().$gambar->getClientOriginalName();
            $gambar->move('public/uploads/dosen/', $new_gambar);

            $dosens_data = [
                'name' => $request->name,
                'detail' => $request->detail,
                'gambar' => 'public/uploads/dosen/'.$new_gambar,
                'slug' => Str::slug($request->name)
            ];    
        }
        else{
            $dosens_data = [
                'name' => $request->name,
                'detail' => $request->detail,
                'slug' => Str::slug($request->name)
            ];
     
        }
        
        $dosens->update($dosens_data);

        return redirect()->route('dosens.index')->with('success', 'Data Berhasil Di Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dosens = Dosens::findorfail($id);
        $dosens->delete();

        return redirect()->back()->with('success', 'Data Berhasil Dihapus');
    }
}
