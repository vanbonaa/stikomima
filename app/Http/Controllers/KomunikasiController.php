<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Communications;

class KomunikasiController extends Controller
{
    public function index(Communications $komunikasi){
        $komunikasi = $komunikasi->latest()->take(4)->get();
        return view('layout.frontend.home', compact('komunikasi'));
    }
    public function list_komunikasi(){
        $komunikasi = Communications::latest()->paginate(6);
        return view('komunikasi.list_komunikasi', compact('komunikasi'));
    }
    public function isi_komunikasi(){
        $komunikasi = Communications::latest()->paginate(6);
        return view('komunikasi.isi_komunikasi', compact('komunikasi'));
    }
}
