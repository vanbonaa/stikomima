<?php

namespace App\Http\Controllers;

use App\Communications;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProdiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $communications = Communications::paginate(10);
        return view('admin.communications.index', compact('communications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.communications.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|min:3'
        ]);
        $communications = Communications::create([
            'name' => $request->name,
            'slug' => Str::slug($request->name)
        ]);
        return redirect()->route('communications.index')->with('success','Program Studi Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $communications = Communications::findorfail($id);
        return view('admin.communications.edit', compact('communications'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);
        $communications_data = [
            'name' => $request->name,
            'slug' => Str::slug($request->name)
        ];

        Communications::whereId($id)->update($communications_data);
        return redirect()->route('communications.index')->with('success','Program Studi Berhasil di Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $communications = Communications::findorfail($id);
        $communications->delete();

        return redirect()->back()->with('success', 'Program Studi Berhasil Dihapus');
    }
}
