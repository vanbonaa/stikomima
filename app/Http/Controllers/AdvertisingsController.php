<?php

namespace App\Http\Controllers;

use App\Advertisings;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AdvertisingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $advertisings = Advertisings::paginate(10);
        return view('admin.advertisings.index', compact('advertisings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.advertisings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'visimisi' => 'required',
            'program' => 'required',
            'karir' => 'required',
            'pdf' => 'required',
            'gambar' => 'required'
        ]);
        $pdf = $request->pdf;
        $new_pdf = time().$pdf->getClientOriginalName();

        $gambar = $request->gambar;
        $new_gambar = time().$gambar->getClientOriginalName();

        $advertisings = Advertisings::create([
            'name' => $request->name,
            'visimisi' => $request->visimisi,
            'program' => $request->program,
            'karir' => $request->karir,
            'pdf' => 'public/uploads/d3/pdf/'.$new_pdf,
            'gambar' => 'public/uploads/d3/gambar/'.$new_gambar,
            'slug' => Str::slug($request->name)
        ]);

        $pdf->move('public/uploads/d3/pdf/', $new_pdf);
        
        $gambar->move('public/uploads/d3/gambar/', $new_gambar);
        return redirect()->route('advertisings.index')->with('success', 'Data Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $advertisings = Advertisings::findorfail($id);
        return view('admin.advertisings.edit', compact('advertisings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'visimisi' => 'required',
            'program' => 'required',
            'karir' => 'required',
        ]);
        $advertisings = Advertisings::findorfail($id);
        if ($request->has('gambar') && $request->has('pdf')) {
            $gambar = $request->gambar;
            $new_gambar = time().$gambar->getClientOriginalName();
            $gambar->move('public/uploads/d3/gambar/', $new_gambar);
                $pdf = $request->pdf;
                $new_pdf = time().$pdf->getClientOriginalName();
                $pdf->move('public/uploads/d3/pdf/', $new_pdf);
            
            $advertisings_data = [
                'name' => $request->name,
                'visimisi' => $request->visimisi,
                'program' => $request->program,
                'karir' => $request->karir,
                'pdf' => 'public/uploads/d3/pdf/'.$new_pdf,
                'gambar' => 'public/uploads/d3/gambar/'.$new_gambar,
                'slug' => Str::slug($request->name)
            ];    
        }
        else if ($request->has('gambar') && ($request->pdf = NULL || $request->pdf =' ')) {
            $gambar = $request->gambar;
            $new_gambar = time().$gambar->getClientOriginalName();
            $gambar->move('public/uploads/d3/gambar/', $new_gambar);
            
            $advertisings_data = [
                'name' => $request->name,
                'visimisi' => $request->visimisi,
                'program' => $request->program,
                'karir' => $request->karir,
                'gambar' => 'public/uploads/d3/gambar/'.$new_gambar,
                'slug' => Str::slug($request->name)
            ];    
        }
        else if ($request->has('pdf') && ($request->gambar = NULL || $request->gambar =' ')) {
            $pdf = $request->pdf;
            $new_pdf = time().$pdf->getClientOriginalName();
            $pdf->move('public/uploads/d3/pdf/', $new_pdf);
            
            $advertisings_data = [
                'name' => $request->name,
                'visimisi' => $request->visimisi,
                'program' => $request->program,
                'karir' => $request->karir,
                'pdf' => 'public/uploads/d3/pdf/'.$new_pdf,
                'slug' => Str::slug($request->name)
            ];    
        }
        else{
            $advertisings_data = [
                'name' => $request->name,
                'visimisi' => $request->visimisi,
                'program' => $request->program,
                'karir' => $request->karir,
                'slug' => Str::slug($request->name)
            ];
        }
            $advertisings->update($advertisings_data);

            return redirect()->route('advertisings.index')->with('success', 'Data Berhasil Di Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $advertisings = Advertisings::findorfail($id);
        $advertisings->delete();

        return redirect()->back()->with('success', 'Data Berhasil Dihapus');
    }
}
