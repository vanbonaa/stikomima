<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Advertisings;

class D3Controller extends Controller
{
    public function index(Advertisings $d3){
        $d3 = $d3->latest()->take(4)->get();
        return view('layout.frontend.home', compact('d3'));
    }
    public function list_komunikasi(){
        $d3 = Advertisings::latest()->paginate(6);
        return view('komunikasi.list_komunikasi', compact('d3'));
    }
    public function isi_d3(){
        $d3 = Advertisings::latest()->paginate(6);
        return view('d3.isi_d3', compact('d3'));
    }
}
