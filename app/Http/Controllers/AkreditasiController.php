<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Akreditations;

class AkreditasiController extends Controller
{
    public function index(Akreditations $akreditasi){
        $akreditasi = $akreditasi->latest()->take(4)->get();
        return view('layout.frontend.home', compact('akreditasi'));
    }
    public function list_akre(){
        $akreditasi = Akreditations::latest()->paginate(6);
        return view('akreditasi.list_akre', compact('akreditasi'));
    }
    public function isi_akre(){
        $akreditasi = Akreditations::latest()->paginate(6);
        return view('akreditasi.isi_akre', compact('akreditasi'));
    }
}
