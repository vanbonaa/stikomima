<?php

namespace App\Http\Controllers;

use App\Visimisions;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class VisimisionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $visimisions = Visimisions::paginate(10);
        return view('admin.visimisions.index', compact('visimisions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.visimisions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'judul' => 'required',
            'visi' => 'required',
            'misi' => 'required',
            'gambar' => 'required'
        ]);

        $gambar = $request->gambar;
        $new_gambar = time().$gambar->getClientOriginalName();

        $visimisions = Visimisions::create([
            'judul' => $request->judul,
            'visi' => $request->visi,
            'misi' => $request->misi,
            'gambar' => 'public/uploads/profile/visimisions/'.$new_gambar,
            'slug' => Str::slug($request->judul)
        ]);
        
        $gambar->move('public/uploads/profile/visimisions/', $new_gambar);
        return redirect()->route('visimisions.index')->with('success', 'Data Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $visimisions = Visimisions::findorfail($id);
        return view('admin.visimisions.edit', compact('visimisions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'judul' => 'required',
            'visi' => 'required',
        ]);
        $visimisions = Visimisions::findorfail($id);
        if ($request->has('gambar')) {
            $gambar = $request->gambar;
            $new_gambar = time().$gambar->getClientOriginalName();
            $gambar->move('public/uploads/profile/visimisions/', $new_gambar);

            $visimisions_data = [
                'judul' => $request->judul,
                'visi' => $request->visi,
                'misi' => $request->misi,
                'gambar' => 'public/uploads/profile/visimisions/'.$new_gambar,
                'slug' => Str::slug($request->judul)
            ];    
        }
        else{
            $visimisions_data = [
                'judul' => $request->judul,
                'visi' => $request->visi,
                'misi' => $request->misi,
                'slug' => Str::slug($request->judul)
            ];
     
        }
        
        $visimisions->update($visimisions_data);

        return redirect()->route('visimisions.index')->with('success', 'Data Berhasil Di Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $visimisions = Visimisions::findorfail($id);
        $visimisions->delete();

        return redirect()->back()->with('success', 'Data Berhasil Dihapus');
    }
}
