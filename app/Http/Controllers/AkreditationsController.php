<?php

namespace App\Http\Controllers;

use App\Akreditations;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AkreditationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $akreditations = Akreditations::paginate(10);
        return view('admin.akreditations.index', compact('akreditations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.akreditations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'status' => 'required',
            'no_surat' => 'required',
            'pdf' => 'required',
            'keterangan' => 'required'
        ]);
        $pdf = $request->pdf;
        $new_pdf = time().$pdf->getClientOriginalName();

        $akreditations = Akreditations::create([
            'name' => $request->name,
            'status' => $request->status,
            'no_surat' => $request->no_surat,
            'pdf' => 'public/uploads/akreditasi/pdf/'.$new_pdf,
            'keterangan' => $request->keterangan,
            'slug' => Str::slug($request->name)
        ]);

        $pdf->move('public/uploads/akreditasi/pdf/', $new_pdf);
        
        return redirect()->route('akreditations.index')->with('success', 'Data Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $akreditations = Akreditations::findorfail($id);
        return view('admin.akreditations.edit', compact('akreditations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'status' => 'required',
            'no_surat' => 'required',
            // 'pdf' => 'required',
            'keterangan' => 'required'
        ]);
        $akreditations = Akreditations::findorfail($id);
        if ($request->has('pdf')) {
                $pdf = $request->pdf;
                $new_pdf = time().$pdf->getClientOriginalName();
                $pdf->move('public/uploads/akreditasi/pdf/', $new_pdf);
            
            $akreditations_data = [
                'name' => $request->name,
                'status' => $request->status,
                'no_surat' => $request->no_surat,
                'pdf' => 'public/uploads/akreditasi/pdf/'.$new_pdf,
                'keterangan' => $request->keterangan,
                'slug' => Str::slug($request->name)
            ];    
        }
        else{
            $akreditations_data = [
                'name' => $request->name,
                'status' => $request->status,
                'no_surat' => $request->no_surat,
                'keterangan' => $request->keterangan,
                'slug' => Str::slug($request->name)
            ];
     
        } 
        $akreditations->update($akreditations_data);

        return redirect()->route('akreditations.index')->with('success', 'Data Berhasil Di Update');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $akreditations = Akreditations::findorfail($id);
        $akreditations->delete();

        return redirect()->back()->with('success', 'Data Berhasil Dihapus');
    }
}
