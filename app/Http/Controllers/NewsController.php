<?php

namespace App\Http\Controllers;

use App\News;
use App\Category;
use App\Tags;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::paginate(10);
        return view('admin.news.index', compact('news'));
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tags::all();
        $category = Category::all();
        return view('admin.news.create', compact('category','tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'judul' => 'required',
            'category_id' => 'required',
            'content' => 'required',
            'pdf' => 'required',
            'gambar' => 'required'
        ]);
        $pdf = $request->pdf;
        $new_pdf = time().$pdf->getClientOriginalName();

        $gambar = $request->gambar;
        $new_gambar = time().$gambar->getClientOriginalName();

        $news = News::create([
            'judul' => $request->judul,
            'category_id' => $request->category_id,
            'content' => $request->content,
            'pdf' => 'public/uploads/news/pdf/'.$new_pdf,
            'gambar' => 'public/uploads/news/gambar/'.$new_gambar,
            'slug' => Str::slug($request->judul)
        ]);

        $news->tags()->attach($request->tags);

        $pdf->move('public/uploads/news/pdf/', $new_pdf);
        
        $gambar->move('public/uploads/news/gambar/', $new_gambar);
        return redirect()->route('news.index')->with('success', 'Data Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::all();
        $tags = Tags::all();
        $news = News::findorfail($id);
        return view('admin.news.edit', compact('news', 'tags', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'judul' => 'required',
            'category_id' => 'required',
            'content' => 'required'
        ]);

        
        $news = News::findorfail($id);
        if ($request->has('gambar') && $request->has('pdf')) {
            $gambar = $request->gambar;
            $new_gambar = time().$gambar->getClientOriginalName();
            $gambar->move('public/uploads/news/gambar/', $new_gambar);
                $pdf = $request->pdf;
                $new_pdf = time().$pdf->getClientOriginalName();
                $pdf->move('public/uploads/news/pdf/', $new_pdf);
            
            $news_data = [
                'judul' => $request->judul,
                'category_id' => $request->category_id,
                'content' => $request->content,
                'pdf' => 'public/uploads/news/pdf/'.$new_pdf,
                'gambar' => 'public/uploads/news/gambar/'.$new_gambar,
                'slug' => Str::slug($request->judul)
            ];    
        }
        else if ($request->has('gambar') && ($request->pdf = NULL || $request->pdf =' ')) {
            $gambar = $request->gambar;
            $new_gambar = time().$gambar->getClientOriginalName();
            $gambar->move('public/uploads/news/gambar/', $new_gambar);
            
            $news_data = [
                'judul' => $request->judul,
                'category_id' => $request->category_id,
                'content' => $request->content,
                'gambar' => 'public/uploads/news/gambar/'.$new_gambar,
                'slug' => Str::slug($request->judul)
            ];    
        }
        else if ($request->has('pdf') && ($request->gambar = NULL || $request->gambar =' ')) {
            $pdf = $request->pdf;
            $new_pdf = time().$pdf->getClientOriginalName();
            $pdf->move('public/uploads/news/pdf/', $new_pdf);
            
            $news_data = [
                'judul' => $request->judul,
                'category_id' => $request->category_id,
                'content' => $request->content,
                'pdf' => 'public/uploads/news/pdf/'.$new_pdf,
                'slug' => Str::slug($request->judul)
            ];    
        }
        else{
            $news_data = [
                'judul' => $request->judul,
                'category_id' => $request->category_id,
                'content' => $request->content,
                'slug' => Str::slug($request->judul)
            ];
     
        }
        
        $news->tags()->sync($request->tags);
        $news->update($news_data);

        return redirect()->route('news.index')->with('success', 'Data Berhasil Di Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news = News::findorfail($id);
        $news->delete();

        return redirect()->back()->with('success', 'Data Berhasil Dihapus');
    }

    
}
