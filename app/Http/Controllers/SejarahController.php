<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Histories;

class SejarahController extends Controller
{
    public function index(Histories $sejarah){
        $sejarah = $sejarah->latest()->take(4)->get();
        return view('layout.frontend.home', compact('sejarah'));
    }
    public function isi_sejarah(){
        $sejarah = Histories::latest()->paginate(6);
        return view('sejarah.isi_sejarah', compact('sejarah'));
    }
}
