<?php

namespace App\Http\Controllers;

// use App\News;
use Illuminate\Http\Request;

class PdfController extends Controller
{
    public function pdf()
    {
        $data['judul'] = "{{judul}}";
        $pdf = \PDF::loadView('pdf', $data);
        return $pdf->download('invoice.pdf');
    }
}
