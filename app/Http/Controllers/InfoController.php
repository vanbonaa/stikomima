<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use App\Category;

class InfoController extends Controller
{
    public function index(News $news){
        $category_widget = Category::all();

        $news = $news->latest()->take(4)->get();
        return view('layout.home', compact('news', 'category_widget'));
    }
    public function isi_news($slug){
        $category_widget = Category::all();
        
        $news = News::where('slug', $slug)->get();
        return view('info.isi_news', compact('news', 'category_widget'));
    }

    public function list_news(){
        $category_widget = Category::all();

        $news = News::latest()->paginate(6);
        return view('info.list_news', compact('news', 'category_widget'));
    }
    public function list_category(category $category){
        $category_widget = Category::all();

        $news = Category::news()->paginate();
        return view('info.list_news', compact('news', 'category_widget'));
    }
    public function cari(request $request){
        $category_widget = Category::all();

        $news = News::where('judul', $request->cari)->orWhere('judul','like','%'.$request->cari.'%')->paginate(5);
        return view('info.list_news', compact('news', 'category_widget'));
    }
}
