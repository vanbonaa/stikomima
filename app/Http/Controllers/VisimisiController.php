<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Visimisions;

class VisimisiController extends Controller
{
    public function index(Visimisions $visimisi){
        $visimisi = $visimisi->latest()->take(4)->get();
        return view('layout.frontend.home', compact('visimisi'));
    }
    public function isi_visimisi(){
        $visimisi = Visimisions::latest()->paginate(6);
        return view('visimisi.isi_visimisi', compact('visimisi'));
    }
}
