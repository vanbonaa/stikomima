<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dosens;

class RuangdosenController extends Controller
{
    public function index(Dosens $dosens){
        $dosens = $dosens->latest()->take(4)->get();
        return view('layout.home', compact('dosens'));
    }
    public function list_dosens(){
        $dosens = Dosens::latest()->paginate(6);
        return view('dosens.list_dosens', compact('dosens'));
    }
}
