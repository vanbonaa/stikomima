<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Communications extends Model
{
    protected $fillable = ['name','visimisi','program','karir','pdf','gambar','slug'];
    
}
