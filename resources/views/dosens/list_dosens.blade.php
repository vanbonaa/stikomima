@extends('layout.frontend.home')
@section('title','List Berita')
@section('content')
<div class="container col-sm-12" style="background-image: url('{{ asset('../img/stikombg.jpeg')}}'); background-size:1500px 2000px">
<div class="container bg-light">
<marquee direction="right" class="bg alert-secondary text-primary" width="auto">
<h6>Selamat Datang di Website STIKOM - IMA</h6></marquee>   
  <div class="container-fluid col-sm-12">
  <div class="row">
    <!-- sidebar-left -->
    
    <!-- content -->
    <div style="background-image: url('{{ asset('../img/stikombg.jpeg')}}'); background-size:2000px 200px no-repeat" class="col-sm-9">
    <!-- <div class="row-sm-1"> -->
    <ul class="list-unstyled">
      <div class="card-header bg-primary">
      <h2 class="mb-0">
          <h5 class="text-center text-white">Ruang Dosen dan Staff STIKOM - IMA</h5>
      </h2>
      </div><br>
      
        <!-- berita -->
        <div class="card-deck row row-cols-1 row-cols-md-3">
        @foreach($dosens as $list_dosens)
          <div class="col mb-4">
            <div class="card">
              <img src="{{asset($list_dosens->gambar)}}" class="card-img-top" width=100 height=200 alt="...">
              <div class="card-body text-center">
                <h5 class="card-title text-center">{{($list_dosens->name)}}</h5></a>
                <a href="{{asset($list_dosens->detail)}}">Tentang {{($list_dosens->name)}} </a>
              </div>
            </div>
          </div>
          @endforeach
        </div>
        <!-- <a href="" class="btn btn-primary btn-sm btn-block">Lihat Semua Berita</a> -->

    

<center>{{ $dosens->links() }}</center>
    
    </ul>
      <!-- <div class="container">
        
      </div> -->
        <!-- berita -->
        <!-- <div class="row row-cols-1 row-cols-md-2">
        
        </div> -->
        
      <!-- <div class="card-deck col-sm-12">
      </div> -->
      
    </div>
    <!-- content -->

      <!-- sidebar kanan -->
      <br>
      <div class="col-sm-3 text-center">
        <h6><strong>Sekolah Tinggi</strong></h6>
        <h6>Ilmu Komunikasi</h6>
          <hr class="featurette-divider bg-primary">
        <h6 class="text-center">Social Media Official</h6>
          <div class="content-center">
            <!--<ul class="sppb-icons-group-list">-->
              <!-- <li id="icon-16003458725" class="text-left"> -->
              <a href="https://bit.ly/3quICEl" aria-label="Whatsapp" rel="noopener noreferrer" target="_blank">
              <span style="font-size: 1.2em; color: Green;">
              <i class="fab fa-whatsapp" aria-hidden="true" title="Whatsapp"></i></span>
              </a>&nbsp;

              <!-- <li id="icon-1600345872595" class=" sppb-text-left"> -->
              <a href="https://www.facebook.com/stikimstikom.ima" aria-label="Instagram" rel="noopener noreferrer" target="_blank">
              <span style="font-size: 1.2em; color: blue;">
              <i class="fab fa-facebook" aria-hidden="true" title="Instagram"></i></span>
              </a>&nbsp;
            
            <!-- <li id="icon-1600345872595" class=" sppb-text-left"> -->
              <a href="https://www.instagram.com/stikomima.official/" aria-label="Instagram" rel="noopener noreferrer" target="_blank">
              <span style="font-size: 1.2em; color: tomato;">
              <i class="fab fa-instagram" aria-hidden="true" title="Instagram"></i></span>
              </a>&nbsp;
            
            <!-- <li id="icon-1600345872596" class=" sppb-text-left"> -->
              <a href="https://twitter.com/stikom_ima" aria-label="Twitter" rel="noopener noreferrer" target="_blank">
              <span style="font-size: 1.2em; color: softblue;">
              <i class="fab fa-twitter" aria-hidden="true" title="Twitter"></i></span>
              </a>&nbsp;
            <!-- <li id="icon-1600345872597" class=" sppb-text-left"> -->
              <a href="https://www.youtube.com/channel/UCQ3jGJryI7BqzukySkI2zXA" aria-label="Youtube" rel="noopener noreferrer" target="_blank">
              <span style="font-size: 1.2em; color: red;">
              <i class="fab fa-youtube-square " aria-hidden="true" title="Youtube"></i></span>
              </a>&nbsp;
            </ul>
          </div>
        <hr class="featurette-divider bg-primary ">
        <div class="text-center">
          <a href="https://pddikti.kemdikbud.go.id">
            <img src="{{ ('img/forlap.png') }}" alt="sinta" height="50px" width="160px">
          </a>
          <a href="http://sinta2.ristekdikti.go.id">
            <img src="{{ ('img/sinta.png') }}" alt="dikti" height="50px" width="160px">
          </a>
          <a href="https://www.banpt.or.id/">
            <img src="{{ ('img/banpt.jpg') }}" alt="banpt" height="50px" width="110px">
          </a>
          <a href="http://www.mediapakem.com/">
            <img src="{{ ('img/mediapakem.png') }}" alt="dikti" height="50px" width="160px">
          </a>
        </div>
      </div>
      <!-- sidebar kanan -->
  <!-- container2 -->
  </div>
  </div>
  </div>
<!-- container1 -->
</div>
</div>
@endsection