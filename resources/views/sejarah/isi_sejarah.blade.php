@extends('layout.frontend.home')
@section('title','Sejarah StikomIMA')
@section('content')
<div class="container col-sm-12"  style="background-image: url('{{ asset('../img/stikombg.jpeg')}}'); background-size:1500px 2000px">
<div class="container bg-light">
<marquee direction="right" class="bg alert-secondary text-primary" width="auto">
<h6>Selamat Datang di Website STIKOM - IMA</h6></marquee>   
  <div class="container-fluid col-sm-12">
  <div class="row">
    <!-- sidebar-left -->
    
    <!-- content -->
    <div class="col-sm-9">
    <!-- <div class="row"> -->
    <ul class="list-unstyled">

      @foreach ($sejarah as $isi_sejarah)
      <div class="container">
        <div class="row">
            <div class="col-6">
            <img src="{{asset($isi_sejarah->gambar)}}" width="100%" height="300" class="mr-3" alt="...">
            </div>
            <div class="col-6">
            <br>
            <h3 class="alert alert-primary text-center">{{($isi_sejarah->name)}}</h3>
            </div>
        </div>
      </div>
            
            <div class="accordion" id="accordionExample">
                  <div class="card">
                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                      <div class="card-body text-center">
                      {!! $isi_sejarah->keterangan !!}
                      </div>
                    </div>
                  </div>
      @endforeach 
    
    </ul>
      
    </div>
    <!-- content -->

      <!-- sidebar kanan -->
      <br>
      <div class="col-sm-3 text-center bg alert-primary">
        <h6><strong>Sekolah Tinggi</strong></h6>
        <h6>Ilmu Komunikasi</h6>
          <hr class="featurette-divider bg-primary">
        <h6 class="text-center">Social Media Official</h6>
          <div class="content-center">
            <!--<ul class="sppb-icons-group-list">-->
              <!-- <li id="icon-16003458725" class="text-left"> -->
              <a href="https://bit.ly/3quICEl" aria-label="Whatsapp" rel="noopener noreferrer" target="_blank">
              <span style="font-size: 1.2em; color: Green;">
              <i class="fab fa-whatsapp" aria-hidden="true" title="Whatsapp"></i></span>
              </a>&nbsp;

            <!-- <li id="icon-1600345872595" class=" sppb-text-left"> -->
              <a href="https://www.facebook.com/stikimstikom.ima" aria-label="Instagram" rel="noopener noreferrer" target="_blank">
              <span style="font-size: 1.2em; color: blue;">
              <i class="fab fa-facebook" aria-hidden="true" title="Instagram"></i></span>
              </a>&nbsp;
            
            <!-- <li id="icon-1600345872595" class=" sppb-text-left"> -->
              <a href="https://www.instagram.com/stikomima.official/" aria-label="Instagram" rel="noopener noreferrer" target="_blank">
              <span style="font-size: 1.2em; color: tomato;">
              <i class="fab fa-instagram" aria-hidden="true" title="Instagram"></i></span>
              </a>&nbsp;
            
            <!-- <li id="icon-1600345872596" class=" sppb-text-left"> -->
              <a href="https://twitter.com/stikom_ima" aria-label="Twitter" rel="noopener noreferrer" target="_blank">
              <span style="font-size: 1.2em; color: softblue;">
              <i class="fab fa-twitter" aria-hidden="true" title="Twitter"></i></span>
              </a>&nbsp;
            <!-- <li id="icon-1600345872597" class=" sppb-text-left"> -->
              <a href="https://www.youtube.com/channel/UCQ3jGJryI7BqzukySkI2zXA" aria-label="Youtube" rel="noopener noreferrer" target="_blank">
              <span style="font-size: 1.2em; color: red;">
              <i class="fab fa-youtube-square " aria-hidden="true" title="Youtube"></i></span>
              </a>&nbsp;
            </ul>
          </div>
        <hr class="featurette-divider bg-primary ">
        <div class="text-center">
          <a href="https://pddikti.kemdikbud.go.id">
            <img src="{{ ('../img/forlap.png') }}" alt="sinta" height="50px" width="160px">
          </a>
          <a href="http://sinta2.ristekdikti.go.id">
            <img src="{{ ('../img/sinta.png') }}" alt="dikti" height="50px" width="160px">
          </a>
          <a href="https://www.banpt.or.id/">
            <img src="{{ ('../img/banpt.jpg') }}" alt="banpt" height="50px" width="110px">
          </a>
          <a href="http://www.mediapakem.com/">
            <img src="{{ ('../img/mediapakem.png') }}" alt="dikti" height="50px" width="160px">
          </a>
        </div>
      </div>
      <!-- sidebar kanan -->
  <!-- container2 -->
  </div>
  </div>
  </div>
<!-- container1 -->
</div>
</div>
@endsection

      