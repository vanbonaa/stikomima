@extends('layout.frontend.home')
@section('title','Ijin Pendirian')
@section('content')
<!-- <div class="container-fluid"> -->
<div class="container col-sm-12" style="background-image: url('{{ asset('../img/stikombg.jpeg')}}'); background-size:1500px 2000px">
<div class="container bg-light">
<marquee direction="right" class="bg alert-secondary text-primary" width="auto">
<h6>Selamat Datang di Website STIKOM - IMA</h6></marquee>   
  <div class="container-fluid col-sm-12">
  <div class="row">
    <!-- sidebar-left -->
    <div class="col-sm-2">
      <div>
        <nav class="nav flex-column">
          <a class="nav-link active {{ Request::is('home')?'active':'' }}"  href="http://mhs.stikom-ima.ac.id/stikom/">Ruang Mahasiswa</a>
          <a class="nav-link" href="{{ route('dosens.list_dosens') }}">Ruang Dosen</a>
          <!-- <a class="nav-link" href="#">Program Studi</a> -->
          <a class="nav-link" href="#">Karir dan Alumni Center</a>
          <a class="nav-link {{ Request::is('layout')?'active':'' }}" href="http://journal.stikom-ima.ac.id/jurnal-stikom/index.php/jikom1">E-Journal</a>
          <a class="nav-link" href="http://perpustakaan.stikom-ima.ac.id/slims2/">Perpustakaan STIKOM</a>
          <!-- <a class="nav-link" href="{{ route('info.list_news') }}">Cek Hasil Ujian PMB</a> -->
          <!-- <a class="nav-link" href="#">Brosur</a> -->
        </nav>
        <hr class="featurette-divider bg-primary">
        <div class="card" style="width: 10rem;">
          <div class="card-body">
            <h5 class="card-title text-center">Informasi PMB Online</h5>
              <p class="card-text"></p>
              <div>
              <a href="http://pmb.stikom-ima.ac.id/" class="btn btn-primary btn-sm content-center">Selengkapnya Tentang PMB</a>
              </div>
              <div>
              <a href="{{ route('info.list_news') }}" class="btn btn-primary btn-sm btn-block">Pengumuman Hasil Ujian PMB</a>
              </div>
          </div>
        </div><br>
      </div>
    </div>

    <!-- content -->
    <div class="col-sm-8">
      <div class=".justify-content-center col-sm">
      <div class="card text-justify">
    <div class="card-header text-center alert bg-primary text-white">
        <h4>Ijin Pendirian</h4>
    </div>
        <div class="card-body text-center text-primary">
            <h5 class="card-title text-center">SK Menteri Pendidikan Nasional</h5><br>
            <h3 class="card-text">No. 110/D/O/2003</h3>
            <h6 class="card-text">Tanggal : 1 Agustus 2003</h6>
            
        </div>
    </div>
        
        <!-- <hr class="featurette-divider bg-primary"> -->
      </div>
    </div>
    <!-- content -->

    <div class="col-sm-2">
      <!-- sidebar kanan -->
          <h6 class="text-center">Sekolah Tinggi Ilmu Komunikasi</h6>
          <hr class="featurette-divider bg-primary">
          <h6 text-center><strong> Social Media Official</strong></h6>
          <div class="content-center">
            <!--<ul class="sppb-icons-group-list">-->
              <!-- <li id="icon-16003458725" class="text-left"> -->
              <a href="https://bit.ly/3quICEl" aria-label="Whatsapp" rel="noopener noreferrer" target="_blank">
              <span style="font-size: 1.2em; color: Green;">
              <i class="fab fa-whatsapp" aria-hidden="true" title="Whatsapp"></i></span>
              </a>&nbsp;
            
            <!-- <li id="icon-1600345872595" class=" sppb-text-left"> -->
              <a href="https://www.facebook.com/stikimstikom.ima" aria-label="Instagram" rel="noopener noreferrer" target="_blank">
              <span style="font-size: 1.2em; color: blue;">
              <i class="fab fa-facebook" aria-hidden="true" title="Instagram"></i></span>
              </a>&nbsp;

            <!-- <li id="icon-1600345872595" class=" sppb-text-left"> -->
              <a href="https://www.instagram.com/stikomima.official/" aria-label="Instagram" rel="noopener noreferrer" target="_blank">
              <span style="font-size: 1.2em; color: tomato;">
              <i class="fab fa-instagram" aria-hidden="true" title="Instagram"></i></span>
              </a>&nbsp;
            
            <!-- <li id="icon-1600345872596" class=" sppb-text-left"> -->
              <a href="https://twitter.com/stikom_ima" aria-label="Twitter" rel="noopener noreferrer" target="_blank">
              <span style="font-size: 1.2em; color: softblue;">
              <i class="fab fa-twitter" aria-hidden="true" title="Twitter"></i></span>
              </a>&nbsp;

            <!-- <li id="icon-1600345872597" class=" sppb-text-left"> -->
              <a href="https://www.youtube.com/channel/UCQ3jGJryI7BqzukySkI2zXA" aria-label="Youtube" rel="noopener noreferrer" target="_blank">
              <span style="font-size: 1.2em; color: red;">
              <i class="fab fa-youtube-square " aria-hidden="true" title="Youtube"></i></span>
              </a>&nbsp;
              
            </ul>
          </div>
            <hr class="featurette-divider bg-primary ">
          <div class="text-center">
          <a href="https://pddikti.kemdikbud.go.id">
            <img src="{{ ('img/forlap.png') }}" alt="sinta" height="50px" width="160px">
          </a>
          <a href="http://sinta2.ristekdikti.go.id">
            <img src="{{ ('img/sinta.png') }}" alt="dikti" height="50px" width="160px">
          </a>
          <a href="https://www.banpt.or.id/">
            <img src="{{ ('img/banpt.jpg') }}" alt="banpt" height="50px" width="110px">
          </a>
          <a href="http://www.mediapakem.com/">
            <img src="{{ ('img/mediapakem.png') }}" alt="dikti" height="50px" width="160px">
          </a>
        </div>
        </div>
      </div> 
    </div>
      <!-- sidebar kanan -->
    </div>
  </div>
</div>
</div>

@endsection