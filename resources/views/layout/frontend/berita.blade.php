@extends('layout.frontend.home')
@section('berita')
@foreach($data as $post_terbaru)
          <div class="col mb-4">
            <div class="card">
              <img src="{{ $post_terbaru->gambar }}" class="card-img-top" height=200 alt="...">
              <div class="card-body">
                <a href="{{ route('blog.isi_post', $post_terbaru->slug)}}">
                <h5 class="card-title text-center">{{ $post_terbaru->judul }}</h5></a>
                <a class="card-text text-center"><small class="text-muted">{{ $post_terbaru->users->name }}</small></a>
            <a class="card-text text-center"><small class="text-muted">{{ $post_terbaru->created_at->diffForHumans() }}</small></a>
              </div>
            </div>
          </div>
          @endforeach

@endsection

<hr class="featurette-divider bg-primary">
      <div class="container">
        <span style="font-size: 1.3em; color: oceanblue;">
          <i class="fas fa-newspaper" aria-hidden="true" title="berita"><strong class="text-black">&nbsp Pengumuman Hasil Ujian</strong></i>
        </span>
        <h6 class="title text-left"></h6>
      </div>
        <!-- berita -->
        <div class="row row-cols-1 row-cols-md-2">
        @foreach($news as $news_terbaru)
          <div class="col mb-4">
            <div class="card">
              <!-- <img src="{{ $news_terbaru->gambar }}" class="card-img-top" height=200 alt="..."> -->
              <div class="card-body">
                <a href="{{ route('info.isi_news', $news_terbaru->slug)}}">
                <h5 class="card-title text-center">{{ $news_terbaru->judul }}</h5></a>
            <a class="card-text text-center"><small class="text-muted">{{ $news_terbaru->created_at->diffForHumans() }}</small></a>
              </div>
            </div>
          </div>
          @endforeach
        </div>
        <a href="{{ route('info.list_news') }}" class="btn btn-primary btn-sm btn-block">Lihat Pengumuman</a>