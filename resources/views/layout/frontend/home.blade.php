<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <script src="https://kit.fontawesome.com/db5e1de913.js" crossorigin="anonymous"></script>
    <link rel="icon" type="image/png" href="{{ ('img/logo1.png') }}">
    <!-- <title> STIKOM IMA</title> -->
    <title>STIKOM-IMA - @yield('title')</title>
    <meta property="og:title" content="STIKOM-IMA"/>
<meta property="og:type" content="website"/>
<meta property="og:description" content="terbaik untuk Indonesia"/>
<meta property="og:url" content="https://stikom-ima.ac.id/"/>
<meta property="og:site_name" content="STIKOM-IMA"/>

<!-- This site is optimized with the Yoast SEO plugin v12.9.1 - https://yoast.com/wordpress/plugins/seo/ -->
<meta name="description" content="Sekolah tinggi ilmu komunikasi IMA (STIKOM IMA) merupakan kampus paling tepat jurusan ilmu komunikasi dan periklanan di Jakarta. Broadcaster, Advertising"/>
<meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1"/>
<link rel="canonical" href="https://stikom-ima.ac.id/" />
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Beranda - STIKOM-IMA" />
<meta property="og:description" content="Sekolah tinggi ilmu komunikasi IMA (STIKOM IMA) merupakan kampus paling tepat jurusan ilmu komunikasi dan periklanan di Jakarta. Broadcaster, Advertising" />
<meta property="og:url" content="https://stikom-ima.ac.id/" />
<meta property="og:site_name" content="STIKOM-IMA" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:description" content="Sekolah tinggi ilmu komunikasi IMA (STIKOM IMA) merupakan kampus paling tepat jurusan ilmu komunikasi dan periklanan di Jakarta. Broadcaster, Advertising" />
<meta name="twitter:title" content="Beranda - STIKOM-IMA" />
<script type='application/ld+json' class='yoast-schema-graph yoast-schema-graph--main'>{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://stikom-ima.ac.id/#website","url":"https://stikom-ima.ac.id/","name":"STIKOM-IMA","description":"terbaik untuk Indonesia","potentialAction":{"@type":"SearchAction","target":"https://stikom-ima.ac.id/?s={search_term_string}","query-input":"required name=search_term_string"}},{"@type":"WebPage","@id":"https://stikom-ima.ac.id/#webpage","url":"https://stikom-ima.ac.id/","inLanguage":"en-US","name":"Beranda - STIKOM-IMA","isPartOf":{"@id":"https://stikom-ima.ac.id/#website"},"datePublished":"2019-09-29T02:28:32+00:00","dateModified":"2020-02-03T06:51:03+00:00","description":"Sekolah tinggi ilmu komunikasi IMA (STIKOM IMA) merupakan kampus paling tepat jurusan ilmu komunikasi dan periklanan di Jakarta. Broadcaster, Advertising"}]}
</script>
  </head>
  <body>
  <header style="background-image: url('{{ asset('../img/stikombg.jpeg')}}'); background-size:2000px 200px">
<!-- navbar -->
@include('layout.frontend.navbar')
<!-- navbar -->

<!-- content -->
@yield('content')
<!-- content -->

<!-- footer -->
@include('layout.frontend.footer')
<!-- footer -->
