<div class="container">
    <!--Grid row-->
    <div class="row">
    <div class="container-marketing">
<div class="container-fluid">
<nav class="navbar navbar-dark navbar-light">
<!-- <img src="{{ ('../img/bgstikom.jpg')}}" class="img-responsive" alt=""> -->
    <div class="media">
      <img src="{{ ('../img/logo1.png') }}" width="180" height="150" class="" alt="">
      <div class="media-body text-primary ml-4" style="text-shadow: -2px -2px 2px white">
        <h4>Sekolah Tinggi Ilmu Komunikasi</h4>
        <h2>Indonesia Maju</h2>
        <h4>(STIKOM-IMA)</h4>
        <h6>The Heart Of Communication School</h6>
      </div>
    </div>
  </header>
    <main role="main">
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="container">
    
  <!-- <a class="navbar-brand" href="#">STIKOM-IMA</a> -->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  
  <div class="collapse navbar-collapse navbar-dark" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link navbar-dark {{ Request::is('home')?'active':'' }}"  href="{{ url('/') }}">Beranda <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle {{ Request::is('layout')?'active':'' }}" href="" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Akreditasi
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item {{ Request::is('layout')?'active':'' }}" href="{{ url('ijin') }}">Ijin Pendirian</a>
          <a class="dropdown-item" href="{{ url('isi_akre') }}">Akreditasi Kampus STIKOM-IMA</a>
          <!-- <div class="dropdown-divider"></div> -->
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle {{ Request::is('layout')?'active':'' }}" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Profil
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="{{ url('isi_sejarah') }}">Sejarah</a>
          <a class="dropdown-item" href="{{ url('isi_visimisi') }}">Visi dan Misi</a>
          <!-- <div class="dropdown-divider"></div> -->
          <!-- <a class="dropdown-item {{ Request::is('layout')?'active':'' }}" href="{{ url('') }}">Unsur Pimpinan</a>
          <a class="dropdown-item {{ Request::is('layout')?'active':'' }}" href="{{ url('') }}">Struktur Organisasi</a> -->
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle {{ Request::is('layout')?'active':'' }}" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Program Studi
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="{{ url('isi_komunikasi') }}">S1 Komunikasi</a>
          <a class="dropdown-item" href="{{ url('isi_d3') }}">D3 Advertising</a>
          <!-- <div class="dropdown-divider"></div> -->
        </div>
      </li>
      <!-- <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Akademik
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Jadwal Kuliah</a>
          <a class="dropdown-item" href="#">Kalender Akademik</a> -->
          <!-- <div class="dropdown-divider"></div> -->
        <!-- </div>
      </li> -->
      <li class="nav-item active">
      <a class="nav-link navbar-dark {{ Request::is('layout')?'active':'' }}" href="http://silaron.stikom-ima.ac.id/">Silaron</a>
      </li>
      <li class="nav-item active">
      <a class="nav-link navbar-dark {{ Request::is('layout')?'active':'' }}" href="{{ url('stikomtv') }}">STIKOM TV</a>
      </li>
      <!-- <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle {{ Request::is('layout')?'active':'' }}" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Ruang Mahasiswa
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item {{ Request::is('layout')?'active':'' }}" href="http://mhs.stikom-ima.ac.id/stikom/">Info Mahasiswa</a>
          <a class="dropdown-item {{ Request::is('layout')?'active':'' }}" href="http://pmb.stikom-ima.ac.id/">PMB STIKOM-IMA</a>
          <a class="dropdown-item {{ Request::is('layout')?'active':'' }}" href="http://journal.stikom-ima.ac.id/jurnal-stikom/index.php/jikom1">Jurnal</a>
          <a class="dropdown-item {{ Request::is('layout')?'active':'' }}" href="http://perpustakaan.stikom-ima.ac.id/slims2/">Perpustakaan STIKOM</a>
          <a class="dropdown-item {{ Request::is('layout')?'active':'' }}" href="http://silaron.stikom-ima.ac.id/">Silaron</a>
          <a class="dropdown-item {{ Request::is('layout')?'active':'' }}" href="#">Fasilitas Kampus</a>
          <div class="dropdown-divider"></div>
        </div>
      </li> -->
    </ul>
    <form class="form-inline my-2 my-lg-0" action="">
    <!-- <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div> -->
      <input class="form-control mr-sm-2" type="search" name="cari" placeholder="Search" aria-label="Search">
      <button class="btn btn-light my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>  
  </div>
</nav>
</div>
</div>
</div>
    