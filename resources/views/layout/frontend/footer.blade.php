

<!-- /END THE FEATURETTES -->


</div>
</div>
  <!-- /.container -->
  <!-- FOOTER -->
<footer class="bg-primary text-white text-lg-start ">
  <!-- Grid container -->
  <div class="container"><br>
    <!--Grid row-->
    <div class="row">
      <!--Grid column-->
      <div class="col-lg-4 col-md-12 mb-4 mb-md-0">
      <span style="font-size: 1em; color: white;">
      <i class="fas fa-map-marker-alt" aria-hidden="true" title="Location">&nbsp;<u class="text-white">Alamat Kami</u></i></span>
        <p>
          Gedung HZ Jln. Harapan Nomor 50,<br>
          Lenteng Agung Jakarta Selatan, 12610
        </p>
        <hr class="featurette-divider navbar-expand-lg navbar-dark bg-primary my-0">
        <span style="font-size: 1em; color: white;">
            <i class="fas fa-mobile-alt" aria-hidden="true" title="mobile">&nbsp;<u class="text-white">Kontak Servis</u></i></span>
              <p>021-78894043 atau 788894044</p> 
          <hr class="featurette-divider navbar-expand-lg navbar-dark bg-primary my-0">
          <span style="font-size: 1em; color: white;">
            <i class="fas fa-fax" aria-hidden="true" title="fax">&nbsp;<u class="text-white">Fax Servis</u></i></span>
              <p>021-78894045</p> 
              <hr class="featurette-divider navbar-expand-lg navbar-dark bg-primary my-0">
          <span style="font-size: 1em; color: white;">
          <i class="fas fa-envelope-open-text" aria-hidden="true" title="fax">&nbsp;<u class="text-white">Email</u></i></span>
              <p>humas@stikom-ima.ac.id</p> 
              <hr class="featurette-divider navbar-expand-lg navbar-dark bg-primary my-0">
          <span style="font-size: 1em; color: white;">
          <i class="fas fa-globe" aria-hidden="true" title="fax">&nbsp;<u class="text-white">Website</u></i></span>
              <p>https://www.stikom-ima.ac.id</p>
      </div>
      <!--Grid column-->

      <!--Grid column-->
      <div class="col-lg-4 col-md-6 mb-4 mb-md-0">
        <h5 class="text-uppercase"><u>Eksternal Link</u></h5>
        <ul class="list-unstyled mb-0">
          <li>
            <a href="https://pddikti.kemdikbud.go.id/" class="text-white">> PDDikti</a>
          </li>
          <li>
            <a href="https://forlap.kemdikbud.go.id/" class="text-white">> ForlapDikti</a>
          </li>
          <li>
            <a href="https://sinta.ristekbrin.go.id/" class="text-white">> Sinta</a>
          </li>
          <li>
            <a href="https://www.banpt.or.id" class="text-white">> Badan Akreditasi Nasional - PT</a>
          </li>
        </ul><br>
        <h5 class="text-uppercase"><u>Internal Link</u></h5>
        <ul class="list-unstyled mb-0">
          <li>
            <a href="http://journal.stikom-ima.ac.id/jurnal-stikom/index.php/jikom1" class="text-white">> Journal STIKOM-IMA</a>
          </li>
          <li>
            <a href="http://pmb.stikom-ima.ac.id/" class="text-white">> PMB Online</a>
          </li>
          <li>
            <a href="http://119.18.159.206:8010/auth/login" class="text-white">> Sister STIKOM-IMA</a>
          </li>
          <li>
            <a href="http://perpustakaan.stikom-ima.ac.id/slims2/" class="text-white">> Perpustakaan STIKOM-IMA</a>
          </li>
          <li>
            <a href="http://silaron.stikom-ima.ac.id/" class="text-white">> Silaron</a>
          </li>
        </ul>
      </div>
      <!--Grid column-->

      <!--Grid column-->
      <div class="col-lg-4 col-md-6 mb-4 mb-md-0">
        <h5 class="text-uppercase mb-0">Lokasi Kampus</h5><br>
        <ul class="list-unstyled">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3965.4614186986437!2d106.83566041431128!3d-6.334221963738881!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69edc97cae1a33%3A0x8654aa618601517f!2sSTIKIM%20%26%20STIKOM-IMA!5e0!3m2!1sid!2sid!4v1610703530283!5m2!1sid!2sid" width="400" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </ul>
      </div>
      <!--Grid column-->
    </div>
    <!--Grid row-->
  </div>
  <!-- Grid container -->
  
  </div>
  <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2)">
  <footer class="container-fluid">

    <h6>© 2021 STIKOM-IMA</h6><p>Sekolah Tinggi Ilmu Komunikasi- Indonesia Maju</p>
  </footer>
</div>
</main>
      

  
    <script src="https://kit.fontawesome.com/db5e1de913.js" crossorigin="anonymous"></script>
    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    -->
  </body>
</html>