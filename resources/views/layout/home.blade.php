@extends('layout.frontend.home')
@section('title','Beranda')
@section('content')
  <!-- <div class="container-fluid"> -->
<div class="container col-sm-12" style="background-image: url('{{ asset('../img/stikombg.jpeg')}}'); background-size:1500px 2000px">
<div class="container bg-light">
<marquee direction="right" class="bg alert-secondary text-primary" width="auto">
<h6>Selamat Datang di Website STIKOM - IMA</h6></marquee>   
  <div class="container-fluid col-sm-12">
  <div class="row">
    <!-- sidebar-left -->
    <div class="col-sm-2">
      <div>
        <nav class="nav flex-column">
          <a class="nav-link active {{ Request::is('home')?'active':'' }}"  href="http://mhs.stikom-ima.ac.id/stikom/">Ruang Mahasiswa</a>
          <a class="nav-link" href="{{ route('dosens.list_dosens') }}">Ruang Dosen</a>
          <!-- <a class="nav-link" href="#">Program Studi</a> -->
          <a class="nav-link" href="#">Karir dan Alumni Center</a>
          <a class="nav-link {{ Request::is('layout')?'active':'' }}" href="http://journal.stikom-ima.ac.id/jurnal-stikom/index.php/jikom1">E-Journal</a>
          <a class="nav-link" href="http://perpustakaan.stikom-ima.ac.id/slims2/">Perpustakaan STIKOM</a>
          <!-- <a class="nav-link" href="{{ route('info.list_news') }}">Cek Hasil Ujian PMB</a> -->
          <!-- <a class="nav-link" href="#">Brosur</a> -->
        </nav>
        <hr class="featurette-divider bg-primary">
        <div class="card" style="width: 10rem;">
          <div class="card-body">
            <h5 class="card-title text-center">Informasi PMB Online</h5>
              <p class="card-text"></p>
              <div>
              <a href="http://pmb.stikom-ima.ac.id/" class="btn btn-primary btn-sm content-center">Selengkapnya Tentang PMB</a>
              </div>
              <div>
              <a href="{{ route('info.list_news') }}" class="btn btn-primary btn-sm btn-block">Pengumuman Hasil Ujian PMB</a>
              </div>
          </div>
        </div><br>
      </div>
    </div>
    <!-- content -->
    <div class="col-sm-8 bg alert-primary" >
    <div class=".justify-content-center">
    <!-- <div class="row-sm-1"> -->
      <div id="carouselExampleInterval" class="carousel slide" data-ride="carousel" style="margin-top:5px;">
        <div class="card">
        <div class="carousel-inner">
        @foreach($data as $key => $slide_baru)
          <!-- <div class="carousel-item{{ $key == 0 ? ' active' : '' }}" data-interval="4000">
            <img src="{{ ('../img/pamflet.jpeg') }}" width="400" height="400" class="d-block w-100" alt="...">
          </div> -->
          <div class="carousel-item{{ $key == 0 ? ' active' : '' }}" data-interval="4000">
            <img src="{{ $slide_baru->gambar }}" width="400" height="400" class="d-block w-100" alt="...">
          </div>
        @endforeach
        <a class="carousel-control-prev" href="#carouselExampleInterval" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleInterval" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
        </div>  
      </div>
        </div>
    </div>
      <!-- <hr class="featurette-divider bg-primary">
      <div class="justify-content-center" >
        <h2 class="featurette-heading text-center">Selamat Datang di Website STIKOM-IMA</h2>
        <p class="lead text-center">
        Website ini adalah sarana komunikasi stakeholder STIKOM-IMA</p>
      </div>       -->
      <hr class="featurette-divider bg-primary">
      <div class="container">
        <span style="font-size: 1.3em; color: oceanblue;">
          <i class="fas fa-newspaper" aria-hidden="true" title="berita"><strong class="text-black">&nbsp Berita Terbaru</strong></i>
        </span>
        <h6 class="title text-left">Artikel Sekolah Tinggi Ilmu Komunikasi</h6>
      </div>
        <!-- berita -->
        <div class="row row-cols-1 row-cols-md-2">
        @foreach($data as $post_terbaru)
          <div class="col mb-4">
            <div class="card">
              <img src="{{ $post_terbaru->gambar }}" class="card-img-top" height=200 alt="...">
              <div class="card-body">
                <a href="{{ route('blog.isi_post', $post_terbaru->slug)}}">
                <h5 class="card-title text-center">{{ $post_terbaru->judul }}</h5></a>
                <a class="card-text text-center"><small class="text-muted"><i class="fas fa-user nav-icon"></i>{{ $post_terbaru->users->name }}</small></a>
            <a class="card-text text-center"><small class="text-muted"><i class="fas fa-clock nav-icon"></i>{{ $post_terbaru->created_at->diffForHumans() }}</small></a>
              </div>
            </div>
          </div>
          @endforeach
        </div>
        <a href="{{ route('blog.list_post') }}" class="btn btn-primary btn-sm btn-block">Lihat Semua Berita</a>

      <div class="card-deck col-sm-12">
<hr class="featurette-divider bg-primary">
      <div class="container">
        <span style="font-size: 1.3em; color: oceanblue;">
          <i class="fas fa-newspaper" aria-hidden="true" title="berita"><strong class="text-black">&nbsp Media Official Fanpage</strong></i>
        </span>
        <h6 class="title text-left"></h6>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-sm-6">
            <!-- <div class="container alert alert-primary text-center"> -->
            <iframe width="280" height="200" src="https://www.youtube-nocookie.com/embed/qUbUmUca5C0?start=3" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            <!-- <marquee direction="right" bgcolor="white" width="auto"><h6>Video Profil STIKOM - IMA</h6></marquee><br><h4>Video Profil</h4>STIKOM - IMA -->
            <!-- </div> -->
          </div>
          <div class="col-sm-4">
            <!-- <div class="textwidget custom-html-widget alert alert-primary text-center"> -->
              <iframe src="https://player.radioforge.com/v2/shoutcast.html?radiotype=shoutcast&amp;radiolink=http://139.162.245.57:8281/&amp;rand=2124905890&amp;bcolor=000000&amp;image=&amp;facebook=&amp;twitter=&amp;title=IMA Radio&amp;artist=" frameborder="0" marginheight="0" marginwidth="0" scrolling="no" width="350" height="200"></iframe>
                <!-- <marquee direction="right" bgcolor="white" width="auto"><h6>Radio Streaming STIKOM - IMA</h6></marquee><br> -->
            <!-- </div> -->
          </div>
        </div><br>
      </div>
      </div>
    </div>
    <!-- content -->

       <!-- sidebar kanan -->
       <br>
      <div class="col-sm-2 text-center">
        <h6><strong>Sekolah Tinggi</strong></h6>
        <h6>Ilmu Komunikasi</h6>
          <hr class="featurette-divider bg-primary">
        <h6 class="text-center">Social Media Official</h6>
          <div class="content-center">
            <!--<ul class="sppb-icons-group-list">-->
              <!-- <li id="icon-16003458725" class="text-left"> -->
              <a href="https://bit.ly/3quICEl" aria-label="Whatsapp" rel="noopener noreferrer" target="_blank">
              <span style="font-size: 1.2em; color: Green;">
              <i class="fab fa-whatsapp" aria-hidden="true" title="Whatsapp"></i></span>
              </a>&nbsp;
            
            <!-- <li id="icon-1600345872595" class=" sppb-text-left"> -->
              <a href="https://www.facebook.com/stikimstikom.ima" aria-label="Instagram" rel="noopener noreferrer" target="_blank">
              <span style="font-size: 1.2em; color: blue;">
              <i class="fab fa-facebook" aria-hidden="true" title="Instagram"></i></span>
              </a>&nbsp;

            <!-- <li id="icon-1600345872595" class=" sppb-text-left"> -->
              <a href="https://www.instagram.com/stikomima.official/" aria-label="Instagram" rel="noopener noreferrer" target="_blank">
              <span style="font-size: 1.2em; color: tomato;">
              <i class="fab fa-instagram" aria-hidden="true" title="Instagram"></i></span>
              </a>&nbsp;
            
            <!-- <li id="icon-1600345872596" class=" sppb-text-left"> -->
              <a href="https://twitter.com/stikom_ima" aria-label="Twitter" rel="noopener noreferrer" target="_blank">
              <span style="font-size: 1.2em; color: softblue;">
              <i class="fab fa-twitter" aria-hidden="true" title="Twitter"></i></span>
              </a>&nbsp;

            <!-- <li id="icon-1600345872597" class=" sppb-text-left"> -->
              <a href="https://www.youtube.com/channel/UCQ3jGJryI7BqzukySkI2zXA" aria-label="Youtube" rel="noopener noreferrer" target="_blank">
              <span style="font-size: 1.2em; color: red;">
              <i class="fab fa-youtube-square " aria-hidden="true" title="Youtube"></i></span>
              </a>&nbsp;
              
            </ul>
          </div>
        <hr class="featurette-divider bg-primary ">
        <div class="text-center">
          <a href="https://pddikti.kemdikbud.go.id">
            <img src="{{ ('img/forlap.png') }}" alt="sinta" height="50px" width="160px">
          </a>
          <a href="http://sinta2.ristekdikti.go.id">
            <img src="{{ ('img/sinta.png') }}" alt="dikti" height="50px" width="160px">
          </a>
          <a href="https://www.banpt.or.id/">
            <img src="{{ ('img/banpt.jpg') }}" alt="banpt" height="50px" width="110px">
          </a>
          <a href="http://www.mediapakem.com/">
            <img src="{{ ('img/mediapakem.png') }}" alt="dikti" height="50px" width="160px">
          </a>
        </div>
      </div>
      <!-- sidebar kanan -->
  <!-- container2 -->
  </div>
  </div>
  </div>
<!-- container1 -->
</div>
</div>
@endsection