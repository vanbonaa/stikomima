@extends('backend.home')
@section('title','STIKOM - IMA')
@section('content')
<div class="container" >
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            <div class="card-header bg-info"><center>You are logged in!</center></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <center>Selamat Datang {{ Auth::user()->name }}</center> 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
