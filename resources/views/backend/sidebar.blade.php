 <!-- Main Sidebar Container -->
 <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
    <img src="{{ ('../img/logo1.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight text-light">STIKOM-IMA</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
          with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{ route('home') }}" class="nav-link">
              <i class="nav-icon fas fa-home"></i>
              <p>Home</p>
            </a>
          </li>
          <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-folder-open"></i>
                <p>
                  Ruang User
                  <i class="fas fa-angle-left right"></i>
                </p>
              </a>
                  <ul class="nav nav-treeview">
                    <li class="nav-item">
                      <a href="{{ route('user.index') }}" class="nav-link">
                        <i class="fas fa-user nav-icon"></i>
                        <p>List User</p>
                      </a>
                    </li>
                  </ul>
                </li>
          
          <!-- <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-folder-open"></i>
              <p>
                Kategori
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="{{ route('category.index') }}" class="nav-link">
                      <i class="fas fa-clipboard nav-icon"></i>
                      <p>List Kategori</p>
                    </a>
                  </li>
                </ul>
              </li> -->
          
          
              <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-folder-open"></i>
                <p>
                  Tag
                  <i class="fas fa-angle-left right"></i>
                </p>
              </a>
                  <ul class="nav nav-treeview">
                    <li class="nav-item">
                      <a href="{{ route('tag.index') }}" class="nav-link">
                        <i class="fas fa-bookmark nav-icon"></i>
                        <p>List Tag</p>
                      </a>
                    </li>
                  </ul>
                </li>
          
            @if(auth()->user()->tipe == 1)
                <li class="nav-header">Admin</li>
                <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="nav-icon fas fa-folder-open"></i>
                  <p>
                    Posts
                    <i class="fas fa-angle-left right"></i>
                  </p>
                </a>
                    <ul class="nav nav-treeview">
                      <li class="nav-item">
                        <a href="{{ route('post.index') }}" class="nav-link">
                          <i class="fas fa-newspaper nav-icon"></i>
                          <p>List Posts</p>
                        </a>
                      </li>
                    </ul>
                  </li>
                  
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      <i class="nav-icon fas fa-folder-open"></i>
                      <p>
                        Program Studi
                        <i class="fas fa-angle-left right"></i>
                      </p>
                    </a>
                        <ul class="nav nav-treeview">
                          <li class="nav-item">
                            <a href="{{ route('communications.index') }}" class="nav-link">
                              <i class="fas fa-clipboard nav-icon"></i>
                              <p>S1 Ilmu Komunikasi</p>
                            </a>
                          </li>
                        </ul>
                        <ul class="nav nav-treeview">
                          <li class="nav-item">
                            <a href="{{ route('advertisings.index') }}" class="nav-link">
                              <i class="fas fa-clipboard nav-icon"></i>
                              <p>D3 Advertising</p>
                            </a>
                          </li>
                        </ul>
                      </li>

                      <li class="nav-item">
                      <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-folder-open"></i>
                        <p>
                          Akreditasi
                          <i class="fas fa-angle-left right"></i>
                        </p>
                      </a>
                          <ul class="nav nav-treeview">
                            <li class="nav-item">
                              <a href="{{ route('akreditations.index') }}" class="nav-link">
                                <i class="fas fa-bookmark nav-icon"></i>
                                <p>List Akreditasi</p>
                              </a>
                            </li>
                          </ul>
                        </li>


                <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="nav-icon fas fa-folder-open"></i>
                  <p>
                    Ruang Dosen
                    <i class="fas fa-angle-left right"></i>
                  </p>
                </a>
                    <ul class="nav nav-treeview">
                      <li class="nav-item">
                        <a href="{{ route('dosens.index') }}" class="nav-link">
                          <i class="fas fa-newspaper nav-icon"></i>
                          <p>List Dosen</p>
                        </a>
                      </li>
                    </ul>
                  </li>

                <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="nav-icon fas fa-folder-open"></i>
                  <p>
                    Profil
                    <i class="fas fa-angle-left right"></i>
                  </p>
                </a>
                    <ul class="nav nav-treeview">
                      <li class="nav-item">
                        <a href="{{ route('histories.index') }}" class="nav-link">
                          <i class="fas fa-newspaper nav-icon"></i>
                          <p>List Histories</p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="{{ route('visimisions.index') }}" class="nav-link">
                          <i class="fas fa-newspaper nav-icon"></i>
                          <p>List Visimisions</p>
                        </a>
                      </li>
                    </ul>
                  </li>
              @else
                <li class="nav-header">Marketing</li>
                <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="nav-icon fas fa-folder-open"></i>
                  <p>
                    Marketing
                    <i class="fas fa-angle-left right"></i>
                  </p>
                </a>
                    <ul class="nav nav-treeview">
                      <li class="nav-item">
                        <a href="{{ route('news.index') }}" class="nav-link">
                          <i class="fas fa-newspaper nav-icon"></i>
                          <p>Pengumuman</p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="{{ route('news.index') }}" class="nav-link">
                          <i class="fas fa-newspaper nav-icon"></i>
                          <p>Surat Kelulusan</p>
                        </a>
                      </li>
                    </ul>
                  </li>
              @endif
            </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>