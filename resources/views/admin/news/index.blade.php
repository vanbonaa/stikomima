@extends('backend.home')
@section('title', 'Maketing - Pengumuman')
@section('content')

    @if(Session::has('success'))
    <div class="alert alert-success" role="alert">
        {{ Session('success') }}
    </div>
    @endif

    <a href="{{ route('news.create') }}" class="btn btn-info btn-sm">Tambah News</a>
    <br><br>
    <table class="table table-striped table-hover table-sm table-bordered text-center">
        <thead>
            <tr>
                <th>No</th>
                <th>Judul</th>
                <th>Kategori</th>
                <th>Tag</th>
                <th>PDF</th>
                <th>Thumbnails</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($news as $result => $hasil)
            <tr>
                <td>{{ $result + $news->firstitem() }}</td>
                <td>{{ $hasil->judul }}</td>
                <td>{{ $hasil->category->name }}</td>
                <td>@foreach($hasil->tags as $tag)
                    <ul>
                        <li>{{ $tag->name }}</li>
                    </ul>
                    @endforeach
                </td>
                
                <td style="text-align: center; vertical-align: middle;"><div class="btn-small-radius"><a href="{{ asset($hasil->pdf) }}" class="fa fa-search" title="Lihat Dokumen" target="_blank"> Lihat</a></div></td>
                <td><img src="{{ asset($hasil->gambar) }}" class="img-fluid" style="width:80px"></td>
                <td>
                    <form action="{{ route('news.destroy', $hasil->id) }}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="{{ route('news.edit', $hasil->id) }}" class="btn btn-sm btn-primary">Edit</a>
                        &nbsp
                        <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{ $news->links() }}
@endsection