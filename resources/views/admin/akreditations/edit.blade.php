@extends('backend.home')
@section('sub-judul', 'Tambah Akreditasi')
@section('content')

    @if(count($errors)>0)
        @foreach($errors->all() as $error)
        <div class="alert alert-danger" role="alert">
            {{ $error }}
        </div>
        @endforeach
    @endif

    @if(Session::has('success'))
    <div class="alert alert-success" role="alert">
        {{ Session('success') }}
    </div>
    @endif

    <form action="{{ route('akreditations.update', $akreditations->id ) }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PATCH')
    <div class="form-group">
        <label for="formGroupExampleInput" name="name">Nama Akreditasi</label>
        <input type="text" class="form-control" id="formGroupExampleInput" name="name" placeholder="Masukkan Prodi" value="{{ $akreditations->name }}">
    </div>
    <div class="form-group">
        <label for="formGroupExampleInput" name="status">Status</label>
        <input type="text" class="form-control" id="formGroupExampleInput" name="status" placeholder="Masukkan Status" value="{{ $akreditations->status }}">
    </div>
    <div class="form-group">
        <label for="formGroupExampleInput" name="no_surat">no_surat</label>
        <input type="text" class="form-control" id="formGroupExampleInput" name="no_surat" placeholder="Masukkan Nomor Surat" value="{{ $akreditations->no_surat }}">
    </div>
    <div class="form-group">
        <label>File PDF</label>
        <input type="file" name="pdf" class="form-control">
    </div>
    <div class="form-group">
        <label>Keterangan</label>
        <textarea name="keterangan" id="editor" class="form-control">{{ $akreditations->keterangan }}</textarea>
    </div>
    <div class="form-group">
        <button class="btn btn-primary btn-block">Update Akreditasi</button>
    </div>
    </form>
<script src="https://cdn.ckeditor.com/ckeditor5/26.0.0/classic/ckeditor.js"></script>
<script>
	ClassicEditor
		.create( document.querySelector( '#editor' ) )
		.then( editor => {
			window.editor = editor;
		} )
		.catch( err => {
			console.error( err.stack );
		} );
</script>

@endsection