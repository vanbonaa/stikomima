@extends('backend.home')
@section('title', 'Akreditasi')
@section('content')

    @if(Session::has('success'))
    <div class="alert alert-success" role="alert">
        {{ Session('success') }}
    </div>
    @endif

    <a href="{{ route('akreditations.create') }}" class="btn btn-info btn-sm">Tambah Akreditasi</a>
    <br><br>
    <table class="table table-striped table-hover table-sm table-bordered text-center">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Status</th>
                <th>No. Surat</th>
                <th>PDF</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($akreditations as $result => $hasil)
            <tr>
                <td>{{ $result + $akreditations->firstitem() }}</td>
                <td>{{ $hasil->name }}</td>
                <td>{{ $hasil->status }}</td>
                <td>{{ $hasil->no_surat }}</td>
                <td style="text-align: center; vertical-align: middle;"><div class="btn-small-radius"><a href="{{ asset($hasil->pdf) }}" class="fa fa-search" title="Lihat Dokumen" target="_blank"> Lihat</a></div></td>
                <td>
                    <form action="{{ route('akreditations.destroy', $hasil->id) }}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="{{ route('akreditations.edit', $hasil->id) }}" class="btn btn-sm btn-primary">Edit</a>
                        &nbsp
                        <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
@endsection