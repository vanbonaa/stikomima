@extends('backend.home')
@section('sub-judul', 'Tambah Akreditasi')
@section('content')

    @if(count($errors)>0)
        @foreach($errors->all() as $error)
        <div class="alert alert-danger" role="alert">
            {{ $error }}
        </div>
        @endforeach
    @endif

    @if(Session::has('success'))
    <div class="alert alert-success" role="alert">
        {{ Session('success') }}
    </div>
    @endif

<form action="{{ route('akreditations.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="formGroupExampleInput" name="name">Nama</label>
        <input type="text" class="form-control" id="formGroupExampleInput" name="name" placeholder="Masukkan Nama Akreditasi">
    </div>
    <div class="form-group">
        <label for="formGroupExampleInput" name="status">Status</label>
        <input type="text" class="form-control" id="formGroupExampleInput" name="status" placeholder="Masukkan Status Akreditasi">
    </div>
    <div class="form-group">
        <label for="formGroupExampleInput" name="no_surat">No. Surat</label>
        <input type="text" class="form-control" id="formGroupExampleInput" name="no_surat" placeholder="Masukkan Nomor Surat">
    </div>
    <div class="form-group">
        <label>File PDF</label>
        <input type="file" name="pdf" class="form-control">
    </div>
    <div class="form-group">
        <label>Keterangan</label>
        <textarea name="keterangan" id="editor" class="form-control"></textarea>
    </div>
    <div class="form-group">
        <button class="btn btn-primary btn-block">Simpan Akreditasi</button>
    </div>
    </form>
<script src="https://cdn.ckeditor.com/ckeditor5/26.0.0/classic/ckeditor.js"></script>
<script>
	ClassicEditor
		.create( document.querySelector( '#editor' ) )
		.then( editor => {
			window.editor = editor;
		} )
		.catch( err => {
			console.error( err.stack );
		} );
</script>

@endsection