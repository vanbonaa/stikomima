@extends('backend.home')
@section('title', 'Visi dan Misi')
@section('content')

    @if(Session::has('success'))
    <div class="alert alert-success" role="alert">
        {{ Session('success') }}
    </div>
    @endif

    <a href="{{ route('visimisions.create') }}" class="btn btn-info btn-sm">Tambah Visi dan Misi</a>
    <br><br>
    <table class="table table-striped table-hover table-sm table-bordered text-center">
        <thead>
            <tr>
                <th>No</th>
                <th>Judul</th>
                <th>Gambar</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($visimisions as $result => $hasil)
            <tr>
                <td>{{ $result + $visimisions->firstitem() }}</td>
                <td>{{ $hasil->judul }}</td>
                <td><img src="{{ asset($hasil->gambar) }}" class="img-fluid" style="width:80px"></td>
                <td>
                    <form action="{{ route('visimisions.destroy', $hasil->id) }}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="{{ route('visimisions.edit', $hasil->id) }}" class="btn btn-sm btn-primary">Edit</a>
                        &nbsp
                        <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
@endsection