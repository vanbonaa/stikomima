@extends('backend.home')
@section('sub-judul', 'Edit Post')
@section('content')

    @if(count($errors)>0)
        @foreach($errors->all() as $error)
        <div class="alert alert-danger" role="alert">
            {{ $error }}
        </div>
        @endforeach
    @endif

    @if(Session::has('success'))
    <div class="alert alert-success" role="alert">
        {{ Session('success') }}
    </div>
    @endif

<form action="{{ route('post.update', $post->id ) }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PATCH')
    <div class="form-group">
        <label>Judul</label>
        <input type="text" class="form-control" name="judul" value="{{ $post->judul }}">
    </div>
    <div class="form-group">
        <label>Kategori</label>
        <select class="form-control" name="category_id">
        <option value="" holder>Pilih Kategori</option>
        @foreach($category as $result)
        <option value="{{ $result->id }}"
        @if ($post->category_id == $result->id)
            selected
        @endif
        >{{ $result->name }} </option>
        @endforeach
        </select>
    </div>

            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Pilih Tag</label>
                  <select class="form-control select2bs4" name="tags[]" multiple="multiple" style="width: 100%">
                    @foreach($tags as $tag)
                    <option value="{{ $tag->id }}"
                    @foreach($post->tags as $value)
                        @if($tag->id == $value->id) 
                        selected
                        @endif
                    @endforeach   
                    >{{ $tag->name }}</option>
                    @endforeach
                  </select>
                </div>

    <div class="form-group">
        <label>Konten</label>
        <textarea name="content" id="editor" class="form-control">{{ $post->content }}</textarea>
    </div>
    <div class="form-group">
        <label>Thumbnails</label>
        <input type="file" name="gambar" class="form-control">
    </div>
    <div class="form-group">
        <button class="btn btn-primary btn-block">Update Post</button>
    </div>
</form>
<script src="https://cdn.ckeditor.com/ckeditor5/26.0.0/classic/ckeditor.js"></script>
<script>
	ClassicEditor
		.create( document.querySelector( '#editor' ) )
		.then( editor => {
			window.editor = editor;
		} )
		.catch( err => {
			console.error( err.stack );
		} );
</script>
@endsection