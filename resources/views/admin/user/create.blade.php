@extends('backend.home')
@section('sub-judul', 'Tambah User')
@section('content')

    @if(count($errors)>0)
        @foreach($errors->all() as $error)
        <div class="alert alert-danger" role="alert">
            {{ $error }}
        </div>
        @endforeach
    @endif

    @if(Session::has('success'))
    <div class="alert alert-success" role="alert">
        {{ Session('success') }}
    </div>
    @endif

<form action="{{ route('user.store') }}" method="POST">
    @csrf
    <div class="form-group">
        <label for="formGroupExampleInput" name="name">Nama user</label>
        <input type="text" class="form-control" id="formGroupExampleInput" name="name">
    </div>
    <div class="form-group">
        <label for="formGroupExampleInput">Email</label>
        <input type="text" class="form-control" id="formGroupExampleInput" name="email">
    </div>
    <div class="form-group">
        <label for="formGroupExampleInput">Tipe User</label>
        <select class="form-control" name="tipe">
            <option value="" holder>Pilih Tipe User</option>
            <option value="1">Administrator</option>
            <option value="0">Marketing</option>
        </select>
    </div>
    <div class="form-group">
        <label for="formGroupExampleInput">Password</label>
        <input type="text" class="form-control" id="formGroupExampleInput" name="password" placeholder="Ingat PASSWORD yang anda buat!">
    </div>
    <div class="form-group">
        <button class="btn btn-primary btn-block">Simpan User</button>
    </div>
</form>
@endsection