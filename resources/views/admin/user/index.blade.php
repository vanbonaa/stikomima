@extends('backend.home')
@section('title', 'Ruang User')
@section('content')

    @if(Session::has('success'))
    <div class="alert alert-success" role="alert">
        {{ Session('success') }}
    </div>
    @endif

    <!-- <a href="{{ route('user.create') }}" class="btn btn-info btn-sm">Tambah</a> -->
    <br><br>
    <table class="table table-striped table-hover table-sm table-bordered text-center">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama User</th>
                <th>Email</th>
                <th>Type</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($user as $result => $hasil)
            <tr>
                <td>{{ $result + $user->firstitem() }}</td>
                <td>{{ $hasil->name }}</td>
                <td>{{ $hasil->email }}</td>
                <td>
                    @if($hasil->tipe)
                        <h6><span class="badge badge-info">Administrator</span></h6>
                        @else
                        <h6><span class="badge badge-warning">Marketing</span></h6>
                    @endif
                </td>
                <td>
                    <form action="{{ route('user.destroy', $hasil->id) }}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="{{ route('user.edit', $hasil->id) }}" class="btn btn-sm btn-primary">Edit</a>
                        &nbsp
                        <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{ $user->links() }} <br><br>
    <marquee direction="left" class="bg alert-warning text-danger" width="auto">
    <h6><strong>Selamat Datang di Ruang User Website STIKOM - IMA &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp ** Peringatan!! : Harap mengedit sesuai user yang telah didaftarkan - Terimakasih **</strong></h6></marquee>
@endsection