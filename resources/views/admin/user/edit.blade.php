@extends('backend.home')
@section('sub-judul', 'Tambah User')
@section('content')

    @if(count($errors)>0)
        @foreach($errors->all() as $error)
        <div class="alert alert-danger" role="alert">
            {{ $error }}
        </div>
        @endforeach
    @endif

    @if(Session::has('success'))
    <div class="alert alert-success" role="alert">
        {{ Session('success') }}
    </div>
    @endif

<form action="{{ route('user.update', $user->id) }}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
        <label for="formGroupExampleInput" name="name">Nama user</label>
        <input type="text" class="form-control" id="formGroupExampleInput" name="name" value="{{ $user->name }}">
    </div>
    <div class="form-group">
        <label for="formGroupExampleInput">Email</label>
        <input type="text" class="form-control" id="formGroupExampleInput" name="email" value="{{ $user->email }}" readonly>
    </div>
    <div class="form-group">
        <label for="formGroupExampleInput">Tipe User</label>
        <select class="form-control" name="tipe">
            <option value="" holder>Pilih Tipe User</option>
            <option value="1" holder
            @if($user->tipe == 1)
            selected
            @endif
            >Administrator</option>
            <option value="0" holder
            @if($user->tipe == 0)
            selected
            @endif
            >Marketing</option>
        </select>
    </div>
    <div class="form-group">
        <label for="formGroupExampleInput">Password</label>
        <input type="text" class="form-control" id="formGroupExampleInput" name="password" placeholder="Ingat PASSWORD yang anda buat!">
    </div>
    <div class="form-group">
        <button class="btn btn-primary btn-block">Update User</button>
    </div>
</form>
@endsection