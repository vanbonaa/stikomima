@extends('backend.home')
@section('sub-judul', 'Edit Profil Dosen')
@section('content')

    @if(count($errors)>0)
        @foreach($errors->all() as $error)
        <div class="alert alert-danger" role="alert">
            {{ $error }}
        </div>
        @endforeach
    @endif

    @if(Session::has('success'))
    <div class="alert alert-success" role="alert">
        {{ Session('success') }}
    </div>
    @endif

    <form action="{{ route('dosens.update', $dosens->id ) }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PATCH')
    <div class="form-group">
        <label for="formGroupExampleInput" name="name">Nama Dosen</label>
        <input type="text" class="form-control" id="formGroupExampleInput" name="name" placeholder="Masukkan Prodi" value="{{ $dosens->name }}">
    </div>
    <div class="form-group">
        <label>Info Dosen</label>
        <textarea name="detail" id="editor" class="form-control">{{ $dosens->detail }}</textarea>
    </div>
    <div class="form-group">
        <label>Foto Dosen</label>
        <input type="file" name="gambar" class="form-control">
    </div>
    <div class="form-group">
        <button class="btn btn-primary btn-block">Simpan Program Studi</button>
    </div>
    </form>
<script src="https://cdn.ckeditor.com/ckeditor5/26.0.0/classic/ckeditor.js"></script>
<script>
	ClassicEditor
		.create( document.querySelector( '#editor' ) )
		.then( editor => {
			window.editor = editor;
		} )
		.catch( err => {
			console.error( err.stack );
		} );
</script>
@endsection