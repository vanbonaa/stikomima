@extends('backend.home')
@section('title', 'Ruang Dosen')
@section('content')

    @if(Session::has('success'))
    <div class="alert alert-success" role="alert">
        {{ Session('success') }}
    </div>
    @endif

    <a href="{{ route('dosens.create') }}" class="btn btn-info btn-sm">Tambah Dosen</a>
    <br><br>
    <table class="table table-striped table-hover table-sm table-bordered text-center">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Dosen</th>
                <th>Foto Dosen</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($dosens as $result => $hasil)
            <tr>
                <td>{{ $result + $dosens->firstitem() }}</td>
                <td>{{ $hasil->name }}</td>
                <td><img src="{{ asset($hasil->gambar) }}" class="img-fluid" style="width:80px"></td>
                <td>
                    <form action="{{ route('dosens.destroy', $hasil->id) }}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="{{ route('dosens.edit', $hasil->id) }}" class="btn btn-sm btn-primary">Edit</a>
                        &nbsp
                        <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
@endsection