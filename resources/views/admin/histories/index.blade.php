@extends('backend.home')
@section('title', 'Histories')
@section('content')

    @if(Session::has('success'))
    <div class="alert alert-success" role="alert">
        {{ Session('success') }}
    </div>
    @endif

    <a href="{{ route('histories.create') }}" class="btn btn-info btn-sm">Tambah Histories</a>
    <br><br>
    <table class="table table-striped table-hover table-sm table-bordered text-center">
        <thead>
            <tr>
                <th>No</th>
                <th>Judul</th>
                <th>Gambar</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($histories as $result => $hasil)
            <tr>
                <td>{{ $result + $histories->firstitem() }}</td>
                <td>{{ $hasil->name }}</td>
                <td><img src="{{ asset($hasil->gambar) }}" class="img-fluid" style="width:80px"></td>
                <td>
                    <form action="{{ route('histories.destroy', $hasil->id) }}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="{{ route('histories.edit', $hasil->id) }}" class="btn btn-sm btn-primary">Edit</a>
                        &nbsp
                        <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
@endsection