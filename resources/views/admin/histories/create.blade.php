@extends('backend.home')
@section('sub-judul', 'Tambah Histories')
@section('content')

    @if(count($errors)>0)
        @foreach($errors->all() as $error)
        <div class="alert alert-danger" role="alert">
            {{ $error }}
        </div>
        @endforeach
    @endif

    @if(Session::has('success'))
    <div class="alert alert-success" role="alert">
        {{ Session('success') }}
    </div>
    @endif

<form action="{{ route('histories.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="formGroupExampleInput" name="name">Judul</label>
        <input type="text" class="form-control" id="formGroupExampleInput" name="name" placeholder="Masukkan Judul">
    </div>
    <div class="form-group">
        <label>Sejarah</label>
        <textarea name="keterangan" id="editor" class="form-control"></textarea>
    </div>
    <div class="form-group">
        <label>Thumbnails</label>
        <input type="file" name="gambar" class="form-control">
    </div>
    <div class="form-group">
        <button class="btn btn-primary btn-block">Simpan Histories</button>
    </div>
    </form>
<script src="https://cdn.ckeditor.com/ckeditor5/26.0.0/classic/ckeditor.js"></script>
<script>
	ClassicEditor
		.create( document.querySelector( '#editor' ) )
		.then( editor => {
			window.editor = editor;
		} )
		.catch( err => {
			console.error( err.stack );
		} );
</script>
@endsection