@extends('backend.home')
@section('title','S1 Komunikasi')
@section('content')

    @if(Session::has('success'))
    <div class="alert alert-success" role="alert">
        {{ Session('success') }}
    </div>
    @endif

    <a href="{{ route('communications.create') }}" class="btn btn-info btn-sm">Tambah Prodi</a>
    <br><br>
    <table class="table table-striped table-hover table-sm table-bordered text-center">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Prodi</th>
                <th>PDF</th>
                <th>Thumbnails</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($communications as $result => $hasil)
            <tr>
                <td>{{ $result + $communications->firstitem() }}</td>
                <td>{{ $hasil->name }}</td>
                <td style="text-align: center; vertical-align: middle;"><div class="btn-small-radius"><a href="{{ asset($hasil->pdf) }}" class="fa fa-search" title="Lihat Dokumen" target="_blank"> Lihat</a></div></td>
                <td><img src="{{ asset($hasil->gambar) }}" class="img-fluid" style="width:80px"></td>
                <td>
                    <form action="{{ route('communications.destroy', $hasil->id) }}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="{{ route('communications.edit', $hasil->id) }}" class="btn btn-sm btn-primary">Edit</a>
                        &nbsp
                        <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
@endsection