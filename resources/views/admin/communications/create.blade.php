@extends('backend.home')
@section('title', 'Tambah S1 Komunikasi')
@section('content')

    @if(count($errors)>0)
        @foreach($errors->all() as $error)
        <div class="alert alert-danger" role="alert">
            {{ $error }}
        </div>
        @endforeach
    @endif

    @if(Session::has('success'))
    <div class="alert alert-success" role="alert">
        {{ Session('success') }}
    </div>
    @endif

<form action="{{ route('communications.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="formGroupExampleInput" name="name">Program Studi</label>
        <input type="text" class="form-control" id="formGroupExampleInput" name="name" placeholder="Masukkan Prodi">
    </div>
    <div class="form-group">
        <label>Visi Misi </label>
        <textarea name="visimisi" id="editor" class="form-control"></textarea>
    </div>
    <div class="form-group">
        <label>Program Pendidikan</label>
        <textarea name="program" id="note" class="form-control"></textarea>
    </div>
    <div class="form-group">
        <label>Peluang Karir</label>
        <textarea name="karir" id="peluang" class="form-control"></textarea>
    </div>
    <div class="form-group">
        <label>File PDF</label>
        <input type="file" name="pdf" class="form-control">
    </div>
    <div class="form-group">
        <label>Thumbnails</label>
        <input type="file" name="gambar" class="form-control">
    </div>
    <div class="form-group">
        <button class="btn btn-primary btn-block">Simpan Program Studi</button>
    </div>
    </form>
<script src="https://cdn.ckeditor.com/ckeditor5/26.0.0/classic/ckeditor.js"></script>
<script>
	ClassicEditor
		.create( document.querySelector( '#editor' ) )
		.then( editor => {
			window.editor = editor;
		} )
		.catch( err => {
			console.error( err.stack );
		} );
</script>
<script>
	ClassicEditor
		.create( document.querySelector( '#note' ) )
		.then( editor => {
			window.editor = editor;
		} )
		.catch( err => {
			console.error( err.stack );
		} );
</script>
<script>
	ClassicEditor
		.create( document.querySelector( '#peluang' ) )
		.then( editor => {
			window.editor = editor;
		} )
		.catch( err => {
			console.error( err.stack );
		} );
</script>
@endsection