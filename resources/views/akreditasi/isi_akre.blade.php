@extends('layout.frontend.home')
@section('title','Akreditasi')
@section('content')
<div class="container col-sm-12"  style="background-image: url('{{ asset('../img/stikombg.jpeg')}}'); background-size:1500px 2000px">
<div class="container bg-light">
<marquee direction="right" class="bg alert-secondary text-primary" width="auto">
<h6>Selamat Datang di Website STIKOM - IMA</h6></marquee>   
  <div class="container-fluid col-sm-12">
  <div class="row">
    <!-- sidebar-left -->
    
    <!-- content -->
    <div class="col-sm-9">
    <!-- <div class="row"> -->
    <ul class="list-unstyled">

            <h3 class="mt-0 mb-1 text-center"></h3><br>
      @foreach ($akreditasi as $isi_akre)
            <div class="accordion" id="accordionExample">
                  <div class="card">
                    <div class="card-header" id="headingOne">
                      <h2 class="mb-0">
                        <h4 class="text-center">{{($isi_akre->name)}}</h4>
                      </h2>
                    </div>
                    <div id="collapse" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                      <div class="card-body text-center">
                      <h4>{!! $isi_akre->status !!}</h4>
                      <h5>{!! $isi_akre->no_surat !!}</h5>
                      <h6>{!! $isi_akre->keterangan !!}</h6>
                      <td style="text-align: left; vertical-align: middle;"><div class="btn-small-radius bg alert-info text-center"><a href="{{ asset($isi_akre->pdf) }}" class="fa fa-search" title="Lihat Dokumen" target="_blank">Lihat Surat Akreditasi</a></div></td><br>
                      </div>
                    </div>
                  </div>
      @endforeach 
    
    </ul>
      
    </div>
    <!-- content -->

      <!-- sidebar kanan -->
      <br>
      <div class="col-sm-3 text-center bg alert-primary">
        <h6><strong>Sekolah Tinggi</strong></h6>
        <h6>Ilmu Komunikasi</h6>
          <hr class="featurette-divider bg-primary">
        <h6 class="text-center">Social Media Official</h6>
          <div class="content-center">
            <!--<ul class="sppb-icons-group-list">-->
              <!-- <li id="icon-16003458725" class="text-left"> -->
              <a href="https://bit.ly/3quICEl" aria-label="Whatsapp" rel="noopener noreferrer" target="_blank">
              <span style="font-size: 1.2em; color: Green;">
              <i class="fab fa-whatsapp" aria-hidden="true" title="Whatsapp"></i></span>
              </a>&nbsp;

              <!-- <li id="icon-1600345872595" class=" sppb-text-left"> -->
              <a href="https://www.facebook.com/stikimstikom.ima" aria-label="Instagram" rel="noopener noreferrer" target="_blank">
              <span style="font-size: 1.2em; color: blue;">
              <i class="fab fa-facebook" aria-hidden="true" title="Instagram"></i></span>
              </a>&nbsp;
            
            <!-- <li id="icon-1600345872595" class=" sppb-text-left"> -->
              <a href="https://www.instagram.com/stikomima.official/" aria-label="Instagram" rel="noopener noreferrer" target="_blank">
              <span style="font-size: 1.2em; color: tomato;">
              <i class="fab fa-instagram" aria-hidden="true" title="Instagram"></i></span>
              </a>&nbsp;
            
            <!-- <li id="icon-1600345872596" class=" sppb-text-left"> -->
              <a href="https://twitter.com/stikom_ima" aria-label="Twitter" rel="noopener noreferrer" target="_blank">
              <span style="font-size: 1.2em; color: softblue;">
              <i class="fab fa-twitter" aria-hidden="true" title="Twitter"></i></span>
              </a>&nbsp;
            <!-- <li id="icon-1600345872597" class=" sppb-text-left"> -->
              <a href="https://www.youtube.com/channel/UCQ3jGJryI7BqzukySkI2zXA" aria-label="Youtube" rel="noopener noreferrer" target="_blank">
              <span style="font-size: 1.2em; color: red;">
              <i class="fab fa-youtube-square " aria-hidden="true" title="Youtube"></i></span>
              </a>&nbsp;
            </ul>
          </div>
        <hr class="featurette-divider bg-primary ">
        <div class="text-center">
          <a href="https://pddikti.kemdikbud.go.id">
            <img src="{{ ('../img/forlap.png') }}" alt="sinta" height="50px" width="160px">
          </a>
          <a href="http://sinta2.ristekdikti.go.id">
            <img src="{{ ('../img/sinta.png') }}" alt="dikti" height="50px" width="160px">
          </a>
          <a href="https://www.banpt.or.id/">
            <img src="{{ ('../img/banpt.jpg') }}" alt="banpt" height="50px" width="110px">
          </a>
          <a href="http://www.mediapakem.com/">
            <img src="{{ ('../img/mediapakem.png') }}" alt="dikti" height="50px" width="160px">
          </a>
        </div>
      </div>
      <!-- sidebar kanan -->
  <!-- container2 -->
  </div>
  </div>
  </div>
<!-- container1 -->
</div>
</div>
@endsection

      