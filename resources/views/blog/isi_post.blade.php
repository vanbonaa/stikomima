@extends('layout.frontend.home')
@section('title','Isi Pengumuman')
@section('content')
<div class="container col-sm-12" style="background-image: url('{{ asset('../img/stikombg.jpeg')}}'); background-size:1500px 2000px">
<div class="container bg-light">
<marquee direction="right" class="bg alert-secondary text-primary" width="auto">
<h6>Selamat Datang di Website STIKOM - IMA</h6></marquee>   
  <div class="container-fluid col-sm-12">
  <div class="row">
    <!-- sidebar-left -->
    
    <!-- content -->
    <div class="col-sm-9">
    <!-- <div class="row"> -->
    <ul class="list-unstyled">

    @foreach ($data as $isi_post)
          <div class="alert alert-primary">
          <h2 class="mt-0 mb-1 text-center">{{($isi_post->judul)}}</h2></a>
            <a class="card-text text-right"><i class="fas fa-user nav-icon"></i>&nbsp&nbsp{{ $isi_post->users->name }}</a>
            &nbsp&nbsp<span class="card-text"><small class="text-muted"><i class="fas fa-clock nav-icon"></i>&nbsp{{ $isi_post->created_at->diffForHumans() }}</small></span>
            <!-- <a href="#"><i class="fas fa-clipboard nav-icon">&nbsp{{ $isi_post->category->name }}</i></a> -->
          </div>
          <div class="container">
            <img src="{{asset($isi_post->gambar)}}" width="100%" height="500" class="mr-3" alt="...">  
          </div><br>
          <div class="text-center ">
          {!! $isi_post->content !!}
          </div>
    @endforeach
      
      </ul><br>
    </div>
    <!-- content -->

      <!-- sidebar kanan -->
      <br>
      <div class="col-sm-3 text-center bg alert-primary">
        <h6><strong>Sekolah Tinggi</strong></h6>
        <h6>Ilmu Komunikasi</h6>
          <hr class="featurette-divider bg-primary">
        <h6 class="text-center">Social Media Official</h6>
          <div class="content-center">
            <!--<ul class="sppb-icons-group-list">-->
              <!-- <li id="icon-16003458725" class="text-left"> -->
              <a href="https://bit.ly/3quICEl" aria-label="Whatsapp" rel="noopener noreferrer" target="_blank">
              <span style="font-size: 1.2em; color: Green;">
              <i class="fab fa-whatsapp" aria-hidden="true" title="Whatsapp"></i></span>
              </a>&nbsp;

              <!-- <li id="icon-1600345872595" class=" sppb-text-left"> -->
              <a href="https://www.facebook.com/stikimstikom.ima" aria-label="Instagram" rel="noopener noreferrer" target="_blank">
              <span style="font-size: 1.2em; color: blue;">
              <i class="fab fa-facebook" aria-hidden="true" title="Instagram"></i></span>
              </a>&nbsp;
            
            <!-- <li id="icon-1600345872595" class=" sppb-text-left"> -->
              <a href="https://www.instagram.com/stikomima.official/" aria-label="Instagram" rel="noopener noreferrer" target="_blank">
              <span style="font-size: 1.2em; color: tomato;">
              <i class="fab fa-instagram" aria-hidden="true" title="Instagram"></i></span>
              </a>&nbsp;
            
            <!-- <li id="icon-1600345872596" class=" sppb-text-left"> -->
              <a href="https://twitter.com/stikom_ima" aria-label="Twitter" rel="noopener noreferrer" target="_blank">
              <span style="font-size: 1.2em; color: softblue;">
              <i class="fab fa-twitter" aria-hidden="true" title="Twitter"></i></span>
              </a>&nbsp;
            <!-- <li id="icon-1600345872597" class=" sppb-text-left"> -->
              <a href="https://www.youtube.com/channel/UCQ3jGJryI7BqzukySkI2zXA" aria-label="Youtube" rel="noopener noreferrer" target="_blank">
              <span style="font-size: 1.2em; color: red;">
              <i class="fab fa-youtube-square " aria-hidden="true" title="Youtube"></i></span>
              </a>&nbsp;
            </ul>
          </div>
        <hr class="featurette-divider bg-primary ">
        <div class="text-center">
          <a href="https://pddikti.kemdikbud.go.id">
            <img src="{{ ('../img/forlap.png') }}" alt="sinta" height="50px" width="160px">
          </a>
          <a href="http://sinta2.ristekdikti.go.id">
            <img src="{{ ('../img/sinta.png') }}" alt="dikti" height="50px" width="160px">
          </a>
          <a href="https://www.banpt.or.id/">
            <img src="{{ ('../img/banpt.jpg') }}" alt="banpt" height="50px" width="110px">
          </a>
          <a href="http://www.mediapakem.com/">
            <img src="{{ ('../img/mediapakem.png') }}" alt="dikti" height="50px" width="160px">
          </a>
        </div>
        <hr class="featurette-divider bg-primary">
        <div class="card" style="width: 15rem;">
          <div class="card-body">
            <h5 class="card-title text-center">Informasi PMB Online</h5>
              <p class="card-text"></p>
              <div>
              <a href="http://pmb.stikom-ima.ac.id/" class="btn btn-primary btn-sm content-center">Selengkapnya Tentang PMB</a>
              </div>
              <div>
              <a href="{{ route('info.list_news') }}" class="btn btn-primary btn-sm btn-block">Pengumuman Hasil Ujian PMB</a>
              </div>
          </div>
        </div><br>
      </div>
      <!-- sidebar kanan -->
  <!-- container2 -->
  </div>
  </div>
  </div>
<!-- container1 -->
</div>
</div>
@endsection