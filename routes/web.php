<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', 'BlogController@index');
// Route::get('/', 'InfoController@index');
// Route::get('/', function () {
    //     return view('welcome');
    // });
    // Route::get('/home', function () {
        //     return view('home');
        // })->name('home');
        // Route::get('/', function() {
            //   return view('layout.home');
            // });
            
            // BERITA TERBARU
            Route::get('/isi_post/{slug}', 'BlogController@isi_post')->name('blog.isi_post');
            Route::get('/list_post', 'BlogController@list_post')->name('blog.list_post');
            Route::get('/category/{category}', 'BlogController@list_category')->name('blog.category');
            Route::get('/cari', 'BlogController@cari')->name('blog.cari');
            

// Pengumuman Terbaru
Route::get('/isi_news/{slug}', 'InfoController@isi_news')->name('info.isi_news');
Route::get('/list_news', 'InfoController@list_news')->name('info.list_news');
Route::get('/category/{category}', 'InfoController@list_category')->name('info.category');

// dosen
Route::get('/list_dosens', 'RuangdosenController@list_dosens')->name('dosens.list_dosens');

Route::get('/list_komunikasi', 'KomunikasiController@list_komunikasi')->name('komunikasi.list_komunikasi');
Route::get('/isi_komunikasi', 'KomunikasiController@isi_komunikasi')->name('komunikasi.isi_komunikasi');

Route::get('/isi_d3', 'D3Controller@isi_d3')->name('d3.isi_d3');

Route::get('/isi_akre', 'AkreditasiController@isi_akre')->name('akreditasi.isi_akre');
Route::get('/list_akre', 'AkreditasiController@list_akre')->name('akreditasi.list_akre');

Route::get('/isi_sejarah', 'SejarahController@isi_sejarah')->name('sejarah.isi_sejarah');

Route::get('/isi_visimisi', 'VisimisiController@isi_visimisi')->name('visimisi.isi_visimisi');



Route::get('/stikomtv', function() {
    return view('layout.stikomtv');
  });

Route::get('/ijin', function() {
    return view('layout.ijin');
  });
  
  Route::group(['middleware' => 'auth'], function(){
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('/category', 'CategoryController');
    Route::resource('/tag', 'TagController');
    Route::resource('/communications', 'CommunicationsController')->middleware('role: 1');
    Route::resource('/advertisings', 'AdvertisingsController')->middleware('role: 1');
    Route::resource('/dosens', 'DosensController')->middleware('role: 1');
    Route::resource('/akreditations', 'AkreditationsController')->middleware('role: 1');
    Route::resource('/histories', 'HistoriesController')->middleware('role: 1');
    Route::resource('/visimisions', 'VisimisionsController')->middleware('role: 1');
    Route::resource('/post', 'PostController')->middleware('role: 1');
    Route::resource('/news', 'NewsController')->middleware('role: 0');
    Route::resource('/user', 'UserController');
    
});
// Route::get('laporan-pdf', 'PdfController@pdf');


